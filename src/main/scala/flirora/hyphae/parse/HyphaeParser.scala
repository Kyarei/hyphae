package flirora.hyphae.parse

import java.io.{File => JFile}

import scala.collection.immutable
import scala.collection.immutable.ArraySeq

import flirora.hyphae.interpret.InterpretLibrary
import flirora.hyphae.interpret.ScopedBlock
import org.parboiled2.{
  CharPredicate,
  ErrorFormatter,
  ParseError,
  Parser,
  ParserInput,
  Rule0,
  Rule1
}
import scala.collection.immutable.TreeMap
import scala.util.Success
import scala.util.Failure
import Parser.DeliveryScheme.Either
import scala.util.parsing.input.Position
import scala.util.parsing.input.OffsetPosition
import scala.util.parsing.input.Positional

case class ParserError(path: Option[JFile], message: String)
    extends CompilationError {
  override def show: String =
    s"Parsing error at ${Utils.prettifyFilename(path)}:\n$message"
}

object HyphaeParser {
  sealed trait AstItem
  sealed trait AstLibraryItem
  case class StatementItem(statement: AstStatement) extends AstItem
  case class FunctionItem(function: AstDefinedFunction) extends AstItem
  case class PragmaItem(pragma: Pragma) extends AstItem
  case class ImportItem(name: String, path: String)
      extends AstItem
      with AstLibraryItem
  case class LibraryFunctionItem(function: LibraryDef[String, RawBlock])
      extends AstLibraryItem

  val debug: Boolean = false

  def parseProgram(
      path: Option[JFile],
      code: String
  ): Either[CompilationError, AstProgram] = {
    if (debug) println(code)
    val parser = new HyphaeParser(code, path)
    parser.program.run() match {
      case Right(result) => Right(result)
      case Left(err) => {
        if (debug)
          println(
            parser.formatError(err, new ErrorFormatter(showTraces = true))
          )
        Left(ParserError(path, err.format(code)))
      }
    }
  }

  def parseExpr(
      path: Option[JFile],
      code: String
  ): Either[CompilationError, AstExpression] = {
    if (debug) println(code)
    val parser = new HyphaeParser(code, path)
    parser.expressionEoi.run() match {
      case Right(result) => Right(result)
      case Left(err) => {
        if (debug)
          println(
            parser.formatError(err, new ErrorFormatter(showTraces = true))
          )
        Left(ParserError(path, err.format(code)))
      }
    }
  }

  def parse1Statement(
      path: Option[JFile],
      code: String
  ): Either[CompilationError, AstStatement] = {
    if (debug) println(code)
    val parser = new HyphaeParser(code, path)
    parser.statementEoi.run() match {
      case Right(result) => Right(result)
      case Left(err) => {
        if (debug)
          println(
            parser.formatError(err, new ErrorFormatter(showTraces = true))
          )
        Left(ParserError(path, err.format(code)))
      }
    }
  }

  def parseLibrary(
      path: Option[JFile],
      code: String
  ): Either[CompilationError, AstLibrary] = {
    if (debug) println(code)
    val parser = new HyphaeParser(code, path)
    parser.library.run() match {
      case Right(result) => Right(result)
      case Left(err) => {
        if (debug)
          println(
            parser.formatError(err, new ErrorFormatter(showTraces = true))
          )
        Left(ParserError(path, err.format(code)))
      }
    }
  }
}

class HyphaeParser(val inputStr: String, val path: Option[JFile])
    extends Parser {
  import HyphaeParser._

  val input: ParserInput = inputStr

  def getPosition: Position = {
    var c = cursor
    while (c > 0 && inputStr.charAt(c - 1) == '\n') c -= 1
    OffsetPosition(inputStr, c)
  }
  def positioned[T <: Positional](t: T): T = t.setPos(getPosition)

  def whitespace: Rule0 =
    rule {
      quiet(
        (
          anyOf(" \t\n") |
            ('#' ~ (CharPredicate.All -- '\n').*)
        ).*
      )
    }

  implicit def wspStr(s: String): Rule0 =
    rule {
      atomic(str(s)).named(s) ~ whitespace
    }

  def digits = rule { CharPredicate.Digit.+ }
  def intLiteral: Rule1[Long] =
    rule {
      capture(
        '-'.? ~ digits
      ) ~> (_.toLong) ~ whitespace
    }
  def numberLiteral: Rule1[AstLiteral[String, RawBlock]] =
    rule {
      capture(
        '-'.? ~ digits ~ ('.' ~ digits).? ~ (anyOf("Ee") ~ anyOf(
          "+-"
        ).? ~ digits).?
      ) ~> { str =>
        str.toLongOption match {
          case Some(n) => AstLiteral[String, RawBlock](IntValue(n))
          case None    => AstLiteral[String, RawBlock](RealValue(str.toDouble))
        }
      } ~ whitespace
    }
  def boolLiteral: Rule1[Boolean] =
    rule {
      "true" ~ push(true) |
        "false" ~ push(false)
    }
  def strLiteral: Rule1[String] =
    rule {
      '"' ~ capture(
        ((CharPredicate.All -- '\\' -- '\"') | ('\\' ~ anyOf("\\\"nt"))).*
      ) ~> (Utils.unescape(_)) ~ '"' ~ whitespace
    }
  def literal: Rule1[AstLiteral[String, RawBlock]] =
    rule {
      (numberLiteral |
        boolLiteral ~> { b => AstLiteral[String, RawBlock](BoolValue(b)) } |
        strLiteral ~> { s: String =>
          AstLiteral[String, RawBlock](StringValue(s))
        } |
        "null" ~ push(AstLiteral[String, RawBlock](NullValue))) ~> (
        (node: AstLiteral[String, RawBlock]) => positioned(node)
      )
    }

  def variable: Rule1[String] =
    rule {
      '$' ~ capture(
        (anyOf("<!")) | ((
          CharPredicate.AlphaNum | '_' | '\\'
        ).* ~ anyOf("?!").?)
      ) ~ whitespace
    }
  def variableExpr: Rule1[Var[String, RawBlock]] =
    rule {
      variable ~> ((name: String) => positioned(Var[String, RawBlock](name)))
    }
  def funcIdentifier: Rule1[String] =
    rule {
      capture(
        ((CharPredicate.Alpha | '_') ~ (
          CharPredicate.AlphaNum | '_' | '\\'
        ).* ~ anyOf("?!").?)
      ) ~ whitespace
    }
  def pathIdentifier: Rule1[Either[Int, String]] =
    rule {
      funcIdentifier ~> (Right(_)) |
        intLiteral ~> ((n: Long) => Left(n.toInt))
    }
  def objectKey: Rule1[String] = rule { strLiteral | funcIdentifier }
  def objectEntry: Rule1[(String, AstExpression)] =
    rule {
      objectKey ~ ":" ~ expression ~> ((k: String, v: AstExpression) => (k, v))
    }

  def parenthesized: Rule1[AstExpression] = rule { "(" ~ expression ~ ")" }
  def list: Rule1[AstExpression] =
    rule {
      "[" ~ ((expression * ",") ~> (elems =>
        positioned(ListConstructor(ArraySeq.from(elems)))
      )) ~ ",".? ~ "]"
    }
  def obj: Rule1[AstExpression] =
    rule {
      "{" ~ (objectEntry * ",") ~ ",".? ~ "}" ~> (
        (elems: Seq[(String, AstExpression)]) =>
          positioned(ObjectConstructor(ArraySeq.from(elems)))
      )
    }

  def functionLiteral: Rule1[FunctionLiteral[String, RawBlock]] =
    rule {
      "def" ~ "[" ~ (funcIdentifier ~> (name =>
        positioned(FunctionLiteral[String, RawBlock](name))
      )) ~ "]"
    }
  def lambda: Rule1[AstExpression] =
    rule {
      "(" ~ (variable * ",") ~ ",".? ~ ")" ~ "->" ~ "{" ~ statements ~ "}" ~> {
        (params: Seq[String], block: RawBlock) =>
          LambdaExpression(ArraySeq.from(params), block, ArraySeq.empty, path)
      }
    }

  def implicitPath: Rule1[AstStore] =
    rule {
      funcIdentifier ~> ((id: String) => Var.fromName(id))
    }
  def implicitPathExpr: Rule1[AstExpression] =
    rule {
      implicitPath ~> ((st: AstStore) => st.deref)
    }

  private val OPS: TreeMap[Int, Map[String, Operator]] =
    Operator.operatorsByPrecedence.map({
      case (precedence, operators) =>
        (precedence, operators.map(o => (o.repr, o)).toMap)
    })
  def operator(precedence: Int): Rule1[Operator] =
    rule {
      valueMap(OPS.getOrElse(precedence, Map.empty)) ~ whitespace
    }
  private val ASSIGN_OPS: Map[String, Operator] = Operator.ALL_OPERATORS
    .filter(_.allowsAssign)
    .map(o => (o.repr + "=", o))
    .toMap
  def operatorAssign: Rule1[Operator] =
    rule {
      valueMap(
        ASSIGN_OPS
      ) ~ whitespace
    }
  private val PREFIX_OPS: Map[String, PrefixOp] =
    PrefixOp.ALL_OPERATORS.map(o => (o.repr, o)).toMap
  def prefixOperator: Rule1[PrefixOp] =
    rule {
      valueMap(PREFIX_OPS) ~ whitespace
    }
  def chain(precedence: Int): Rule1[AstExpression] =
    rule {
      opInvoke(precedence) ~ (operator(precedence) ~ opInvoke(precedence) ~> {
        (a: AstExpression, op: Operator, b: AstExpression) =>
          positioned(OpInvoke(op, a, b))
      }).*
    }

  def opInvoke(precedence: Int): Rule1[AstExpression] = {
    Operator.operatorsByPrecedence.minAfter(precedence + 1) match {
      case Some((nextPrecedence, _)) => chain(nextPrecedence)
      case None                      => term2
    }
  }
  def unaryOpInvoke: Rule1[AstExpression] =
    rule {
      prefixOperator ~ term1 ~> ((op: PrefixOp, expr: AstExpression) =>
        positioned(UnaryOpInvoke(op, expr))
      )
    }
  def functionCall: Rule1[AstExpression] =
    rule {
      funcIdentifier ~ "(" ~ (expression * ",") ~ ",".? ~ ")" ~> {
        (name: String, args: Seq[AstExpression]) =>
          positioned(FunctionCall(name, ArraySeq.from(args)))
      }
    }

  def declare: Rule1[AstStatement] =
    rule {
      "let" ~ variable ~ "=" ~ expression ~> {
        (dest: String, src: AstExpression) =>
          positioned(Declare[String, RawBlock](dest, src))
      }
    }
  def assign: Rule1[AstStatement] =
    rule {
      store ~ "=" ~ expression ~> { (dest: AstStore, src: AstExpression) =>
        positioned(Assign[String, RawBlock](dest, src))
      }
    }
  def assignOp: Rule1[AstStatement] =
    rule {
      store ~ operatorAssign ~ expression ~> {
        (dest: AstStore, op: Operator, src: AstExpression) =>
          positioned(OpAssign[String, RawBlock](dest, src, op))
      }
    }
  def ifStatement: Rule1[AstStatement] =
    rule {
      "if" ~ expression ~ "{" ~ statements ~ "}" ~ ("else" ~ "{" ~ statements ~ "}").? ~> {
        (cond: AstExpression, left: RawBlock, right: Option[RawBlock]) =>
          positioned(right match {
            case None =>
              IfStatement[String, RawBlock](
                cond,
                left,
                RawBlock(ArraySeq.empty)
              )
            case Some(r) => IfStatement[String, RawBlock](cond, left, r)
          })
      }
    }
  def forStatement: Rule1[AstStatement] =
    rule {
      "for" ~ expression ~ "{" ~ statements ~ "}" ~> {
        (over: AstExpression, block: RawBlock) =>
          positioned(ForStatement(over, block, "<", ""))
      }
    }
  def withStatement: Rule1[AstStatement] =
    rule {
      "with" ~ expression ~ "{" ~ statements ~ "}" ~> {
        (on: AstExpression, block: RawBlock) =>
          positioned(WithStatement(on, block, ""))
      }
    }
  def trace: Rule1[AstStatement] =
    rule {
      "trace" ~ expression ~> ((expr: AstExpression) =>
        positioned(TraceStatement[String, RawBlock](expr))
      )
    }
  def returnStatement: Rule1[AstStatement] =
    rule {
      "return" ~ ("=" ~ expression).? ~> { (expr: Option[AstExpression]) =>
        positioned(
          ReturnStatement[String, RawBlock](
            expr.getOrElse(AstLiteral(NullValue))
          )
        )
      }
    }
  def discardStatement: Rule1[AstStatement] =
    rule { "discard" ~ push(positioned(DiscardStatement[String, RawBlock]())) }

  def term2: Rule1[AstExpression] =
    rule { (unaryOpInvoke | term1) ~> ((ex: AstExpression) => positioned(ex)) }
  def term1: Rule1[AstExpression] =
    rule {
      term ~ ("(" ~ (expression * ",") ~ ",".? ~ ")" ~> {
        (f: AstExpression, args: Seq[AstExpression]) =>
          positioned(IndirectFunctionCall(f, ArraySeq.from(args)))
      }).*
    }
  def term: Rule1[AstExpression] =
    rule {
      termr ~ (
        "." ~ pathIdentifier ~> (
          (lhs: AstExpression, pathId: Either[Int, String]) =>
            positioned(DotPath(lhs, pathId))
        ) |
          "[" ~ expression ~ "]" ~> (
            (lhs: AstExpression, index: AstExpression) =>
              positioned(IndexPath(lhs, index))
          )
      ).*
    }
  def termr: Rule1[AstExpression] =
    rule {
      literal | functionCall | functionLiteral | variableExpr | implicitPathExpr | parenthesized | list | obj
    }
  def expression: Rule1[AstExpression] =
    rule {
      lambda | opInvoke(Int.MinValue)
    }
  def expressionEoi: Rule1[AstExpression] =
    rule {
      whitespace ~ expression ~ EOI
    }
  def store: Rule1[AstStore] =
    rule {
      storer ~ (
        "." ~ pathIdentifier ~> {
          (lhs: AstStore, pathId: Either[Int, String]) =>
            positioned(StoreDotPath(lhs, pathId))
        } |
          "[" ~ expression ~ "]" ~> { (lhs: AstStore, index: AstExpression) =>
            positioned(StoreIndexPath(lhs, index))
          }
      ).*
    }
  def storer: Rule1[AstStore] =
    rule {
      variableExpr | implicitPath
    }
  def statement: Rule1[AstStatement] =
    rule {
      ifStatement | forStatement | withStatement | returnStatement | trace | declare | assign | assignOp | discardStatement
    }
  def statementEoi: Rule1[AstStatement] =
    rule {
      whitespace ~ statement ~ EOI
    }
  def statements: Rule1[RawBlock] =
    rule {
      statement.* ~> ((statements: Seq[AstStatement]) =>
        positioned(RawBlock(ArraySeq.from(statements)))
      )
    }

  def function: Rule1[AstDefinedFunction] =
    rule {
      "def" ~ funcIdentifier ~ "(" ~ (variable * ",") ~ ",".? ~ ")" ~ "{" ~ statements ~ "}" ~> {
        (funcName: String, params: Seq[String], body: RawBlock) =>
          positioned(DefinedFunction(funcName, params, body, path))
      }
    }
  def pragma: Rule1[Pragma] =
    rule {
      "pragma" ~ funcIdentifier ~ ("=" ~ literal).? ~> {
        (id: String, value: Option[AstLiteral[String, RawBlock]]) =>
          Pragma(id, value.map(_.value).getOrElse(BoolValue(true)))
      }
    }
  def importStatement: Rule1[ImportItem] =
    rule {
      "import" ~ funcIdentifier ~ "=" ~ strLiteral ~> {
        (name: String, path: String) => ImportItem(name, path)
      }
    }

  def program: Rule1[AstProgram] =
    rule {
      whitespace ~ (
        statement ~> ((s: AstStatement) => StatementItem(s)) |
          function ~> ((f: AstDefinedFunction) => FunctionItem(f)) |
          pragma ~> ((p: Pragma) => PragmaItem(p)) |
          importStatement
      ).* ~ EOI ~> { (lines: Seq[AstItem]) =>
        val statements = lines.collect({ case StatementItem(s) => s })
        val functions = lines.collect({ case FunctionItem(f) => f })
        val pragmata = lines.collect({ case PragmaItem(p) => p })
        val imports = lines.collect({ case ImportItem(k, v) => (k, v) })
        Program[String, RawBlock](
          RawBlock(ArraySeq.from(statements)),
          immutable.HashMap.from(
            functions
              .map(f => (f.name, f))
          ),
          pragmata.map(p => (p.name, p.value)).toMap,
          imports.toList
        )
      }
    }

  def libraryFunction: Rule1[LibraryDef[String, RawBlock]] =
    rule {
      capture("export").? ~ function ~> {
        (token: Option[String], func: AstDefinedFunction) =>
          LibraryDef(func, token.isDefined)
      }
    }
  def library: Rule1[AstLibrary] =
    rule {
      whitespace ~ (
        importStatement |
          libraryFunction ~> ((f: LibraryDef[String, RawBlock]) =>
            LibraryFunctionItem(f)
          )
      ).* ~ EOI ~> { (lines: Seq[AstLibraryItem]) =>
        val functions = lines.collect({ case LibraryFunctionItem(d) => d })
        val imports = lines.collect({ case ImportItem(k, v) => (k, v) })
        Library[String, RawBlock](
          path,
          functions.map((d) => (d.func.name, d)).toMap,
          imports.toList
        )
      }
    }
}
