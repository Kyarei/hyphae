package flirora.hyphae.parse

import flirora.hyphae.interpret.{
  Environment,
  ExecResult,
  InterpretLibrary,
  Returned,
  ScopedBlock,
  Success
}
import flirora.hyphae.parse.ValueRef.Context

import java.io.{File => JFile}
import scala.util.parsing.input.{Position, Positional}
import flirora.hyphae.interpret.Errored

trait AbstractHFunction[V, B] extends Positional {
  val name: String

  def eval(environment: Environment, args: Iterable[Value], callPos: Position)(
      implicit
      ev: V =:= Int,
      ev2: B =:= ScopedBlock
  ): ExecResult[Value]
}

case class DefinedFunction[V, B](
    name: String,
    params: Seq[V],
    body: B,
    fileLocation: Option[JFile]
) extends AbstractHFunction[V, B]
    with Positional {
  override def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    environment.pushCallFrame(name, callPos, fileLocation)
    environment.pushScope(body.localSize)
    val res = for {
      m <-
        Utils
          .mapOrFailResult(
            params.zip(args),
            { p: (V, Value) =>
              environment.setVar(p._1, p._2, ctx)
            }
          )
          .map(_ => NullValue)
      result <- body.exec(environment) match {
        case Returned(x) => Success(x)
        case e           => e.map(_ => NullValue)
      }
    } yield result
    environment.popScope(body.localSize)
    environment.popCallFrame()
    res
  }
}

case class LibraryFunction[V, B](
    library: Library[Int, ScopedBlock],
    func: DefinedFunction[Int, ScopedBlock],
    name: String
) extends AbstractHFunction[V, B] {
  override def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    environment.pushCallFrame(name, callPos, library.fileLocation)
    environment.pushScope(func.body.localSize)
    environment.pushLibraryFrame(Some(library.asInstanceOf[InterpretLibrary]))
    val res = for {
      m <-
        Utils
          .mapOrFailResult(
            func.params.zip(args),
            { p: (Int, Value) =>
              environment.setVar(p._1, p._2, ctx)
            }
          )
          .map(_ => NullValue)
      result <- func.body.exec(environment) match {
        case Returned(x) => Success(x)
        case e           => e.map(_ => NullValue)
      }
    } yield result
    environment.popLibraryFrame()
    environment.popScope(func.body.localSize)
    environment.popCallFrame()
    res
  }
}
