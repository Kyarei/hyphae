package flirora.hyphae.interpret.interop

import flirora.hyphae.interpret._
import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.parse.{RuntimeError, Value}
import flirora.hyphae.transcode.TranscodingError

import scala.util.parsing.input.{NoPosition, Position}

trait IntrinsicFunction extends InterpretHFunction {
  def evalOrTypeError(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  )(implicit
      ev: Int =:= Int,
      ev2: ScopedBlock =:= ScopedBlock
  ): Either[TranscodingError, ExecResult[Value]]

  val signature: String

  override def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  )(implicit
      ev: Int =:= Int,
      ev2: ScopedBlock =:= ScopedBlock
  ): ExecResult[Value] = {
    val ctx = Context(environment, NoPosition)
    environment.pushCallFrame(name, callPos, None)
    val res = evalOrTypeError(environment, args, callPos) match {
      case Left(typeErr) =>
        Errored(
          RuntimeError(
            ctx,
            s"call did not fit signature $signature: ${typeErr.message}"
          )
        )
      case Right(result) => result
    }
    environment.popCallFrame()
    res
  }

  def |(that: IntrinsicFunction): IntrinsicFunction = {
    val _name = name
    val me = this
    new IntrinsicFunction {
      override def evalOrTypeError(
          environment: Environment,
          args: Iterable[Value],
          callPos: Position
      )(implicit
          ev: Int =:= Int,
          ev2: ScopedBlock
            =:= ScopedBlock
      ): Either[TranscodingError, ExecResult[Value]] =
        me.evalOrTypeError(environment, args, callPos)
          .left
          .flatMap(_ => that.evalOrTypeError(environment, args, callPos))

      override val signature: String = s"${me.signature} or ${that.signature}"

      override val name: String = _name
    }
  }
}
