package flirora.hyphae.parse

import flirora.hyphae.interpret.{Errored, ExecResult, Success}
import flirora.hyphae.parse.ValueRef.Context

abstract class PrefixOp(val repr: String) {
  def eval(a: Value): Either[String, Value]

  def evalAsResult(a: Value, ctx: Context): ExecResult[Value] =
    eval(a) match {
      case Right(value) => Success(value)
      case Left(msg) =>
        Errored(RuntimeError(ctx, msg))
    }
}

object PrefixOp {
  case object Plus extends PrefixOp("+") {
    override def eval(a: Value): Either[String, Value] = a match {
      case StringValue(s) =>
        s.toLongOption match {
          case Some(n) => Right(IntValue(n))
          case None =>
            s.toDoubleOption match {
              case Some(x) => Right(RealValue(x))
              case None    => Left(s"$s cannot be coerced into a number")
            }
        }
      case IntValue(n)  => Right(IntValue(n))
      case RealValue(x) => Right(RealValue(x))
      case BoolValue(b) => Right(IntValue(if (b) 1 else 0))
      case _            => Left(s"${a.stringify} cannot be coerced into a number")
    }
  }

  case object Minus extends PrefixOp("-") {
    override def eval(a: Value): Either[String, Value] = a match {
      case IntValue(n)  => Right(IntValue(-n))
      case RealValue(x) => Right(RealValue(-x))
      case _            => Left(s"${a.getType} is not a number")
    }
  }

  case object Not extends PrefixOp("!") {
    override def eval(a: Value): Either[String, Value] = a match {
      case BoolValue(b) => Right(BoolValue(!b))
      case _            => Left(s"${a.stringify} is a ${a.getType}, not a number")
    }
  }

  case object Len extends PrefixOp("%") {
    override def eval(a: Value): Either[String, Value] = a match {
      case StringValue(s) => Right(IntValue(s.length))
      case ListValue(l)   => Right(IntValue(l.length))
      case _              => Left(s"expected string or list, got ${a.getType}")
    }
  }

  case object Str extends PrefixOp("~") {
    override def eval(a: Value): Either[String, Value] =
      Right(StringValue(a.stringify))
  }

  val ALL_OPERATORS: List[PrefixOp] = List(Plus, Minus, Not, Len, Str)
  lazy val operatorsByRepr: Map[String, PrefixOp] = ALL_OPERATORS
    .map(op => op.repr -> op)
    .toMap
}
