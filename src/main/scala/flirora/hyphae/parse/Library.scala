package flirora.hyphae.parse

import java.io.{File => JFile}

final case class LibraryDef[V, B](
    func: DefinedFunction[V, B],
    exported: Boolean
)

final case class Library[V, B](
    fileLocation: Option[JFile],
    definitions: Map[String, LibraryDef[V, B]],
    imports: List[(String, String)]
)
