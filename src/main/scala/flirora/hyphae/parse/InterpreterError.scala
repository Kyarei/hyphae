package flirora.hyphae.parse

import flirora.hyphae.parse.ValueRef.Context

import scala.collection.immutable.ArraySeq

trait InterpreterError {
  def show: String
}

trait CompilationError extends InterpreterError

trait BaseRuntimeError extends InterpreterError

case class RuntimeError(ctx: Context, message: String)
    extends BaseRuntimeError {
  private val functionName = ctx.environment.callFrames.top.functionName
  private val stackTraceStrings: Seq[String] =
    if (ctx.environment.callFrames.length < 2)
      Seq.empty
    else
      ctx.environment.callFrames
        .sliding(2)
        .map(ArraySeq.from)
        .map({ e =>
          s"\n${e(1).functionName} @${Utils
            .prettifyFilename(e(1).fileLocation)}:${e(0).lastPosition}"
        })
        .toSeq

  override def show: String =
    s"${Utils.prettifyFilename(ctx.environment.file)}:${ctx.pos}:$functionName: $message" +
      stackTraceStrings.mkString
}
