package flirora.hyphae.parse

import scala.util.parsing.input.Positional

final case class Pragma(name: String, value: Value) extends Positional
