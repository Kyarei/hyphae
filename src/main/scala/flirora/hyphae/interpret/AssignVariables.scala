package flirora.hyphae.interpret

import java.io.{File => JFile}

import flirora.hyphae.parse._

import scala.collection.immutable.ArraySeq
import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.Positional
import scala.util.parsing.input.Position
import scala.collection.mutable

case class ScopedBlock(
    statements: ArraySeq[InterpretStatement],
    localStart: Int,
    localSize: Int
) extends Positional {
  def exec(environment: Environment): ExecResult[Unit] =
    Utils
      .mapOrFailResult(
        statements,
        (statement: InterpretStatement) => statement.exec(environment)
      )
      .map(_ => ())
}

case class RemapContext(
    nLocals: Int,
    locals: Map[String, Int],
    file: Option[JFile],
    errorsOut: ArrayBuffer[CompilationError]
) {
  def lookup(name: String, errPos: Position): Int =
    locals.getOrElse(
      name, {
        errorsOut += ValidateError(
          file,
          errPos,
          s"variable $$$name not found"
        )
        RemapContext.Invalid
      }
    )

  def addVar(name: String): RemapContext =
    RemapContext(
      nLocals + 1,
      locals + ((name, nLocals)),
      file,
      errorsOut
    )

  def addVars(names: Seq[String]): RemapContext =
    RemapContext(
      nLocals + names.size,
      locals ++ names.zipWithIndex.map(e => (e._1, e._2 + nLocals)),
      file,
      errorsOut
    )
}

object RemapContext {
  val Invalid: Int = -1

  def startMain(file: Option[JFile], errorsOut: ArrayBuffer[CompilationError]) =
    RemapContext(1, Map("" -> 0), file, errorsOut)

  def startFunc(
      params: Seq[String],
      file: Option[JFile],
      errorsOut: ArrayBuffer[CompilationError]
  ) =
    RemapContext(params.size, params.zipWithIndex.toMap, file, errorsOut)
}

object AssignVariables {
  val Invalid: Int = -1

  private case class LambdaVarIndexStep1(index: Int, captured: Boolean)
  private case class LambdaBlockStep1(
      statements: ArraySeq[Statement[LambdaVarIndexStep1, LambdaBlockStep1]],
      localStart: Int,
      localSize: Int
  ) extends Positional

  private def transformLambdaBlockStep1(
      block: ScopedBlock,
      nParameters: Int,
      parameterStart: Int,
      captureMap: mutable.HashMap[Int, Int],
      captures: mutable.ArrayBuffer[Int]
  ): LambdaBlockStep1 = {
    def varTransform(vi: Int): LambdaVarIndexStep1 = {
      if (vi >= parameterStart) LambdaVarIndexStep1(vi - parameterStart, false)
      else {
        captureMap.get(vi) match {
          case Some(res) => LambdaVarIndexStep1(res, true)
          case None => {
            val nCaptures = captures.size
            captureMap.put(vi, nParameters + nCaptures)
            captures.addOne(vi)
            LambdaVarIndexStep1(nCaptures, true)
          }
        }
      }
    }
    val transformedStatements = block.statements.map(
      _.map(
        varTransform,
        varTransform,
        (
            block =>
              transformLambdaBlockStep1(
                block,
                nParameters,
                parameterStart,
                captureMap,
                captures
              )
        )
      )
    )
    LambdaBlockStep1(
      transformedStatements,
      block.localStart - parameterStart,
      block.localSize
    )
  }

  private def transformLambdaBlockStep2(
      block: LambdaBlockStep1,
      nCaptures: Int
  ): ScopedBlock = {
    def varTransform(vi: LambdaVarIndexStep1): Int =
      vi match {
        case LambdaVarIndexStep1(i, false) => i + nCaptures
        case LambdaVarIndexStep1(i, true)  => i
      }
    val transformedStatements = block.statements.map(
      _.map(
        varTransform,
        varTransform,
        (block => transformLambdaBlockStep2(block, nCaptures))
      )
    )
    ScopedBlock(
      transformedStatements,
      block.localStart,
      block.localSize + nCaptures
    )
  }

  /**
    * Final steps for transforming a block of a lambda.
    *
    * This remaps the variables in [parameterStart, ∞) (which are assumed
    * to represent parameters or local variables) to the correct range by
    * subtracting parameterStart, and marks variables from enclosing scopes
    * to be captured.
    *
    * @param block the block to perform the transformation step on
    * @param nParameters the number of parameters of this lambda
    * @param parameterStart the index of the first parameter of this
    *   lambda before this transformation step
    * @return a pair `(newBlock, captures)`, where `newBlock` is the
    *   transformed block and `captures` is a sequence of variables
    *   that should be captured by this lambda
    */
  def transformLambdaBlock(
      block: ScopedBlock,
      nParameters: Int,
      parameterStart: Int
  ): (ScopedBlock, IndexedSeq[Int]) = {
    // Step 1: Create separate indices for inner and outer vars
    var captureMap: mutable.HashMap[Int, Int] = mutable.HashMap.empty
    val captures: mutable.ArrayBuffer[Int] = mutable.ArrayBuffer.empty
    val transformed1 = transformLambdaBlockStep1(
      block,
      nParameters,
      parameterStart,
      captureMap,
      captures
    )
    // Step 2: Increment every inner var by the number of captured vars
    val transformed2 = transformLambdaBlockStep2(transformed1, captures.size)
    (transformed2, captures.toIndexedSeq)
  }

  def transformExpression(expr: AstExpression)(implicit
      remapContext: RemapContext
  ): InterpretExpression = {
    val expr1: InterpretExpression = expr match {
      case AstLiteral(value)  => AstLiteral(value)
      case DotPath(obj, path) => DotPath(transformExpression(obj), path)
      case FunctionCall(name, args) =>
        FunctionCall(name, args.map(transformExpression(_)))
      case FunctionLiteral(name) => FunctionLiteral(name)
      case IndexPath(obj, path) =>
        IndexPath(transformExpression(obj), transformExpression(path))
      case IndirectFunctionCall(functionHandle, args) =>
        IndirectFunctionCall(
          transformExpression(functionHandle),
          args.map(transformExpression(_))
        )
      case LambdaExpression(parameters, body, captures, file) => {
        assert(captures.isEmpty)
        val nParameters = parameters.size
        val parameterStart = remapContext.nLocals
        val newRemapContext = remapContext.addVars(parameters)
        val intermediaryBody =
          transformBlock(body, nParameters)(newRemapContext)
        val (finalBody, finalCaptures) =
          transformLambdaBlock(intermediaryBody, nParameters, parameterStart)
        LambdaExpression(
          ArraySeq.from(0 until nParameters),
          finalBody,
          ArraySeq.from(finalCaptures.map(Var(_))),
          file
        )
      }
      case ListConstructor(elements) =>
        ListConstructor(elements.map(transformExpression(_)))
      case ObjectConstructor(elements) =>
        ObjectConstructor(elements.map({
          case (k, v) => (k, transformExpression(v))
        }))
      case OpInvoke(op, left, right) =>
        OpInvoke(op, transformExpression(left), transformExpression(right))
      case UnaryOpInvoke(op, expr) =>
        UnaryOpInvoke(op, transformExpression(expr))
      case v @ Var(name) => Var(remapContext.lookup(name, v.pos))
    }
    expr1.setPos(expr.pos)
  }

  def transformStore(
      store: AstStore
  )(implicit remapContext: RemapContext): InterpretStore = {
    val store1: InterpretStore = store match {
      case v @ Var(name)           => Var(remapContext.lookup(name, v.pos))
      case StoreDotPath(obj, path) => StoreDotPath(transformStore(obj), path)
      case StoreIndexPath(obj, path) =>
        StoreIndexPath(transformStore(obj), transformExpression(path))
    }
    store1.setPos(store.pos)
  }

  def transformStatement(statement: AstStatement)(implicit
      remapContext: RemapContext
  ): (InterpretStatement, RemapContext) = {
    val (statement1: InterpretStatement, ctx1) = statement match {
      case Assign(dest, src) =>
        (Assign(transformStore(dest), transformExpression(src)), remapContext)
      case Declare(dest, src) => {
        val newSrc = transformExpression(src)
        val newRemapContext = remapContext.addVar(dest)
        (Declare(remapContext.nLocals, newSrc), newRemapContext)
      }
      case DiscardStatement() =>
        (DiscardStatement[Int, ScopedBlock](), remapContext)
      case ForStatement(over, block, keyVar, valVar) => {
        val newRemapContext = remapContext.addVar(keyVar).addVar(valVar)
        (
          ForStatement(
            transformExpression(over),
            transformBlock(block, 2)(newRemapContext),
            remapContext.nLocals,
            remapContext.nLocals + 1
          ),
          remapContext
        )
      }
      case IfStatement(cond, block, block2) =>
        (
          IfStatement(
            transformExpression(cond),
            transformBlock(block, 0),
            transformBlock(block2, 0)
          ),
          remapContext
        )
      case OpAssign(dest, src, op) =>
        (
          OpAssign(transformStore(dest), transformExpression(src), op),
          remapContext
        )
      case ReturnStatement(expr) =>
        (ReturnStatement(transformExpression(expr)), remapContext)
      case TraceStatement(expr) =>
        (TraceStatement(transformExpression(expr)), remapContext)
      case WithStatement(on, block, withVar) => {
        val newRemapContext = remapContext.addVar(withVar)
        (
          WithStatement(
            transformExpression(on),
            transformBlock(block, 1)(newRemapContext),
            remapContext.nLocals
          ),
          remapContext
        )
      }
    }
    (statement1.setPos(statement.pos), ctx1)
  }

  def transformBlock(
      block: RawBlock,
      extras: Int
  )(implicit remapContext: RemapContext): ScopedBlock = {
    val (statements, newRemapContext) = Utils.mapWithState(
      block.statements,
      remapContext,
      { (statement: AstStatement, ctx: RemapContext) =>
        transformStatement(statement)(ctx)
      }
    )
    ScopedBlock(
      ArraySeq.from(statements),
      remapContext.nLocals - extras,
      newRemapContext.nLocals - remapContext.nLocals + extras
    ).setPos(block.pos)
  }

  def transform(
      program: RawBlock,
      file: Option[JFile],
      errorsOut: ArrayBuffer[CompilationError]
  ): ScopedBlock =
    transformBlock(program, 1)(RemapContext.startMain(file, errorsOut))

  def transform(
      function: AstDefinedFunction,
      file: Option[JFile],
      errorsOut: ArrayBuffer[CompilationError]
  ): InterpretDefinedFunction = {
    val transformedBlock = transformBlock(function.body, function.params.size)(
      RemapContext.startFunc(function.params, file, errorsOut)
    )
    DefinedFunction(
      function.name,
      0 until function.params.size,
      transformedBlock,
      function.fileLocation
    ).setPos(function.pos)
  }

  def transform(
      program: AstProgram,
      file: Option[JFile],
      // Sorry, I had to cheat here with a mutable list for errors,
      // or I would've been here for way longer.
      errorsOut: ArrayBuffer[CompilationError]
  ): InterpretProgram =
    Program(
      transform(program.main, file, errorsOut),
      program.definedFunctions.map[String, InterpretDefinedFunction]({
        case (name, f) => (name, transform(f, file, errorsOut))
      }),
      program.pragmata,
      program.imports
    )

  def transform(
      library: AstLibrary,
      file: Option[JFile],
      errorsOut: ArrayBuffer[CompilationError]
  ): InterpretLibrary =
    Library(
      library.fileLocation,
      library.definitions.map({
        case (name, libraryDef) =>
          (
            name,
            LibraryDef(
              transform(libraryDef.func, file, errorsOut),
              libraryDef.exported
            )
          )
      }),
      library.imports
    )
}
