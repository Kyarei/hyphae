package flirora.hyphae.parse

import flirora.hyphae.interpret._
import flirora.hyphae.parse.ValueRef.Context

import scala.collection.immutable.ArraySeq
import scala.util.parsing.input.Positional

sealed trait Statement[V, B] extends Positional {
  // You can usually treat varTransform and outerVarTransform the same unless
  // you're introducing new variables to scope.
  def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): Statement[V2, B2]

  // def visitBlocksAndExpressions[V2, B2](exprTransform: Expression[V, B] => Expression[V2, B2], outerExprTransform: Expression[V, B] => Expression[V2, B2], blockTransform: B => B2): Statement[V2, B2]

  def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit]

  def extras: Seq[V] = Seq.empty
}

case class RawBlock(statements: ArraySeq[AstStatement]) extends Positional

object RawBlock {
  def of(elems: AstStatement*): RawBlock = RawBlock(ArraySeq.from(elems))
}

case class Declare[V, B](dest: V, src: Expression[V, B])
    extends Statement[V, B] {
  def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): Declare[V2, B2] =
    Declare(varTransform(dest), src.map(outerVarTransform, blockTransform))
      .setPos(pos)

  override def exec(environment: Environment)(implicit
      ev: V =:= Int,
      ev2: B
        =:= ScopedBlock
  ): ExecResult[Unit] =
    src
      .eval(environment)
      .map(environment.setVar(dest, _, Context(environment, pos)))

  override def extras: Seq[V] = Seq(dest)
}

case class Assign[V, B](dest: Store[V, B], src: Expression[V, B])
    extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): Assign[V2, B2] =
    Assign(
      dest.map(varTransform, blockTransform),
      src.map(varTransform, blockTransform)
    ).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] =
    for {
      srcRes <- src.eval(environment)
      destRes <- dest.evalAsStore(environment)
      result <- destRes.set(srcRes, Context(environment, pos))
    } yield result
}

case class OpAssign[V, B](
    dest: Store[V, B],
    src: Expression[V, B],
    op: Operator
) extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): OpAssign[V2, B2] =
    OpAssign(
      dest.map(varTransform, blockTransform),
      src.map(varTransform, blockTransform),
      op
    ).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] = {
    val ctx = Context(environment, pos)
    op match {
      case eager: EvaluableOperator =>
        for {
          srcRes <- src.eval(environment)
          destRes <- dest.evalAsStore(environment)
          destVal <- destRes.get(ctx)
          withOp <-
            eager
              .evalMutableAsResult(destVal, srcRes, ctx)
          result <- destRes.set(withOp, ctx)
        } yield result
      case _ => Errored(RuntimeError(ctx, "impossible"))
    }
  }
}

case class IfStatement[V, B](cond: Expression[V, B], block: B, block2: B)
    extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): IfStatement[V2, B2] =
    IfStatement(
      cond.map(varTransform, blockTransform),
      blockTransform(block),
      blockTransform(block2)
    ).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] = {
    val ctx = Context(environment, pos)
    for {
      condRes <- cond.eval(environment)
      b <- Utils.tryBool(ctx, condRes)
      result <-
        if (b) {
          environment.pushScope(block.localSize)
          val res = block.exec(environment)
          environment.popScope(block.localSize)
          res
        } else {
          environment.pushScope(block2.localSize)
          val res = block2.exec(environment)
          environment.popScope(block2.localSize)
          res
        }
    } yield result
  }
}

case class ForStatement[V, B](
    over: Expression[V, B],
    block: B,
    keyVar: V,
    valVar: V
) extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): ForStatement[V2, B2] =
    ForStatement(
      over.map(outerVarTransform, blockTransform),
      blockTransform(block),
      varTransform(keyVar),
      varTransform(valVar)
    ).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] = {
    val ctx = Context(environment, pos)
    for {
      overRes <- over.eval(environment)
      result <- overRes match {
        case l @ ListValue(list) => {
          environment.pushScope(block.localSize)
          val res = Utils.mapOrFailResult(
            list.zipWithIndex,
            { p: (Value, Int) =>
              environment.setRef(keyVar, BoxedValue(IntValue(p._2)))
              environment.setRef(valVar, IndexedValueRef(l, p._2))
              environment.pushLoopFrame()
              val res = block.exec(environment)
              val frame = environment.popLoopFrame()
              res.map(_ => if (frame.discarded) Some(p._2) else None)
            }
          )
          environment.popScope(block.localSize)
          res.map({ res =>
            val discarded = res.flatten
            Utils.removeBySortedIndicesInPlace(list, discarded)
          })
        }
        case o @ ObjectValue(obj) => {
          environment.pushScope(block.localSize)
          val res = Utils.mapOrFailResult(
            obj,
            { p: (String, Value) =>
              environment.setRef(keyVar, BoxedValue(StringValue(p._1)))
              environment.setRef(valVar, KeyedValueRef(o, p._1))
              environment.pushLoopFrame()
              val res = block.exec(environment)
              val frame = environment.popLoopFrame()
              res.map(_ => if (frame.discarded) Some(p._1) else None)
            }
          )
          environment.popScope(block.localSize)
          res.map({ res =>
            val discarded = res.flatten
            obj.subtractAll(discarded)
            ()
          })
        }
        case _ =>
          Errored(RuntimeError(ctx, s"cannot iterate over $overRes"))
      }
    } yield result
  }

  override def extras: Seq[V] = Seq(keyVar, valVar)
}

case class WithStatement[V, B](on: Expression[V, B], block: B, withVar: V)
    extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): WithStatement[V2, B2] =
    WithStatement(
      on.map(outerVarTransform, blockTransform),
      blockTransform(block),
      varTransform(withVar)
    ).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] = {
    val ctx = Context(environment, pos)
    for {
      onRes <- on.eval(environment)
      result <- {
        environment.pushScope(block.localSize)
        environment.setVar(withVar, onRes, ctx)
        val res = block.exec(environment)
        environment.popScope(block.localSize)
        res
      }
    } yield result
  }

  override def extras: Seq[V] = Seq(withVar)
}

case class TraceStatement[V, B](expr: Expression[V, B])
    extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): TraceStatement[V2, B2] =
    TraceStatement(expr.map(varTransform, blockTransform)).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] =
    for {
      exprRes <- expr.eval(environment)
    } yield environment.trace(exprRes)
}

case class ReturnStatement[V, B](expr: Expression[V, B])
    extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): Statement[V2, B2] =
    ReturnStatement(expr.map(varTransform, blockTransform)).setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] =
    expr.eval(environment).flatMap(Returned(_))
}

case class DiscardStatement[V, B]() extends Statement[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      outerVarTransform: V => V2,
      blockTransform: B => B2
  ): Statement[V2, B2] =
    DiscardStatement().setPos(pos)

  override def exec(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Unit] = {
    val ctx = Context(environment, pos)
    environment.discardOnCurrentLoopFrame(ctx)
  }
}
