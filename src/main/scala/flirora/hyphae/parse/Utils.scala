package flirora.hyphae.parse

import java.io.{File => JFile}

import flirora.hyphae.interpret.{Errored, ExecResult, Success}
import flirora.hyphae.parse.ValueRef.Context

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
import scala.reflect.runtime.{universe => ru}
import scala.collection.immutable.ArraySeq

object Utils {
  def mapOrFail[A, L, R](
      seq: Iterable[A],
      func: A => Either[L, R]
  ): Either[L, Iterable[R]] = {
    Right(
      for (elem <- seq)
        yield func(elem) match {
          case Left(l)  => return Left(l)
          case Right(r) => r
        }
    )
  }
  def mapOrFailResult[A, R](
      seq: Iterable[A],
      func: A => ExecResult[R]
  ): ExecResult[Iterable[R]] = {
    Success(
      for (elem <- seq)
        yield func(elem) match {
          case Success(r) => r
          case fail       => return fail.map(Iterable.single)
        }
    )
  }

  def unescape(str: String): String = {
    val result = new StringBuilder()
    var i = 0
    while (i < str.length) {
      val c = str(i)
      if (c != '\\') result += c
      else {
        val d = str(i + 1)
        d match {
          case '\\' => result += '\\'
          case '"'  => result += '"'
          case 'n'  => result += '\n'
          case 't'  => result += '\t'
        }
        i += 1
      }
      i += 1
    }
    result.result()
  }

  def prettifyFilename(source: Option[JFile]): String =
    source match {
      case Some(s) => s.getPath
      case None    => "<unknown>"
    }

  def trace[T](x: T): T = {
    println(x)
    x
  }

  def indexedRef(
      ctx: Context,
      v: Value,
      k: Either[Int, String]
  ): ExecResult[ValueRef] =
    (k, v) match {
      case (Left(i), lv: ListValue) => Success(IndexedValueRef(lv, i))
      case (Right(_), _: ListValue) =>
        Errored(RuntimeError(ctx, s"cannot index list with string"))
      case (Left(i), m: ObjectValue)  => Success(KeyedValueRef(m, i.toString))
      case (Right(s), m: ObjectValue) => Success(KeyedValueRef(m, s))
      case _                          => Errored(RuntimeError(ctx, s"cannot index ${v.getType}"))
    }

  def indexedRef(ctx: Context, v: Value, k: Value): ExecResult[ValueRef] =
    indexedRef(
      ctx,
      v,
      k match {
        case IntValue(n) => Left(n.toInt)
        case _           => Right(k.stringify)
      }
    )

  def indexed(
      ctx: Context,
      v: Value,
      k: Either[Int, String]
  ): ExecResult[Value] =
    indexedRef(ctx, v, k).flatMap(_.get(ctx))

  def indexed(ctx: Context, v: Value, k: Value): ExecResult[Value] =
    indexedRef(ctx, v, k).flatMap(_.get(ctx))

  def tryBool(ctx: Context, cond: Value): ExecResult[Boolean] =
    cond match {
      case BoolValue(b) => Success(b)
      case _ =>
        Errored(RuntimeError(ctx, s"cannot cast ${cond.getType} to a boolean"))
    }

  def removeBySortedIndicesInPlace[A](
      list: ArrayBuffer[A],
      discarded: Iterable[Int]
  ): Unit = {
    var destIdx = 0
    var srcIdx = 0
    var discardedIter = discarded.toList
    for ((_, i) <- list.iterator.zipWithIndex) {
      discardedIter match {
        case `i` :: rest => {
          srcIdx += 1
          discardedIter = rest
        }
        case _ => {
          list(destIdx) = list(srcIdx)
          destIdx += 1
          srcIdx += 1
        }
      }
    }
    list.patchInPlace(destIdx, Seq(), srcIdx - destIdx)
  }

  def typeToClassTag[T: ru.TypeTag]: ClassTag[T] =
    ClassTag[T](ru.typeTag[T].mirror.runtimeClass(ru.typeTag[T].tpe))

  def mapWithState[A, B, A1](
      seq: Seq[A],
      seed: B,
      f: (A, B) => (A1, B)
  ): (Seq[A1], B) = {
    var state = seed
    val res: ArrayBuffer[A1] = ArrayBuffer()
    for (elem <- seq) {
      val (newElem, newState) = f(elem, state)
      state = newState
      res.append(newElem)
    }
    (res.toSeq, state)
  }
}
