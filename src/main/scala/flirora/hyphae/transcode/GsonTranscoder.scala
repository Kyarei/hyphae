package flirora.hyphae.transcode

import java.util.Map.Entry

import scala.jdk.CollectionConverters._
import com.google.gson.{
  JsonArray,
  JsonElement,
  JsonNull,
  JsonObject,
  JsonPrimitive
}
import flirora.hyphae.parse._
import java.lang

import com.google.gson.internal.LazilyParsedNumber

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case object GsonTranscoder extends Transcoder[JsonElement] {
  override def toValue(obj: JsonElement): Either[TranscodingError, Value] =
    obj match {
      case _: JsonNull => Right(NullValue)
      case primitive: JsonPrimitive => {
        if (primitive.isString) Right(StringValue(primitive.getAsString))
        else if (primitive.isBoolean) Right(BoolValue(primitive.getAsBoolean))
        else {
          val number = primitive.getAsNumber
          number match {
            case _: Integer | _: lang.Long | _: lang.Short | _: lang.Byte =>
              Right(IntValue(primitive.getAsLong))
            case _: lang.Float | _: lang.Double =>
              Right(RealValue(primitive.getAsDouble))
            case lpn: LazilyParsedNumber =>
              lpn.toString.toLongOption match {
                case Some(n) => Right(IntValue(n))
                case None    => Right(RealValue(lpn.toString.toDouble))
              }
            case _ =>
              Left(
                TranscodingError(
                  s"Number $number of type ${number.getClass.getName} is not supported"
                )
              )
          }
        }
      }
      case array: JsonArray => {
        Utils
          .mapOrFail(array.asScala, toValue)
          .map(l => ListValue(ArrayBuffer.from(l)))
      }
      case obj: JsonObject => {
        Utils
          .mapOrFail(
            obj.entrySet.asScala,
            { e: Entry[String, JsonElement] =>
              toValue(e.getValue).map((e.getKey, _))
            }
          )
          .map(m => ObjectValue(mutable.HashMap.from(m)))
      }
      case _ => Left(TranscodingError(s"unhandled element type $obj"))
    }

  override def fromValue(value: Value): Either[TranscodingError, JsonElement] =
    value match {
      case NullValue      => Right(JsonNull.INSTANCE)
      case BoolValue(b)   => Right(new JsonPrimitive(b))
      case IntValue(n)    => Right(new JsonPrimitive(n))
      case RealValue(x)   => Right(new JsonPrimitive(x))
      case StringValue(s) => Right(new JsonPrimitive(s))
      case ListValue(l) => {
        Utils
          .mapOrFail(l, fromValue)
          .map(it => {
            val array = new JsonArray()
            for (elem <- it) array.add(elem)
            array
          })
      }
      case ObjectValue(o) => {
        Utils
          .mapOrFail(
            o,
            { e: (String, Value) =>
              fromValue(e._2).map((e._1, _))
            }
          )
          .map(it => {
            val obj = new JsonObject
            for ((k, v) <- it) obj.add(k, v)
            obj
          })
      }
      case _ => Left(TranscodingError(s"could not transcribe value $value"))
    }

  override val typename: String = "json element"
}
