package flirora.hyphae.transcode

import flirora.hyphae.interpret.{Errored, ExecResult, Success}
import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.parse.{BaseRuntimeError, RuntimeError, Value}

case class TranscodingError(message: String) extends BaseRuntimeError {
  override def show: String =
    s"Transcoding failed: $message"
  def decorate(ctx: Context): RuntimeError = RuntimeError(ctx, message)
}

trait Transcoder[T] {
  def toValue(obj: T): Either[TranscodingError, Value]
  def fromValue(value: Value): Either[TranscodingError, T]

  def toValueR(obj: T, ctx: Context): ExecResult[Value] = toValue(obj) match {
    case Left(err)    => Errored(err.decorate(ctx))
    case Right(value) => Success(value)
  }

  def fromValueR(value: Value, ctx: Context): ExecResult[T] =
    fromValue(value) match {
      case Left(err)  => Errored(err.decorate(ctx))
      case Right(obj) => Success(obj)
    }

  val typename: String
}
