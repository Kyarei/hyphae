package flirora.hyphae.interpret.interop

import flirora.hyphae.interpret.InterpretHFunction
import flirora.hyphae.parse.{IntValue, StringValue, Value}

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import flirora.hyphae.interpret.Environment
import flirora.hyphae.parse.NullValue

class IntrinsicFunctionRegistry(marshallerRegistry: MarshallerRegistry) {
  private val functions: mutable.HashMap[String, InterpretHFunction] =
    mutable.HashMap()

  def register(func: InterpretHFunction): Unit = {
    if (functions.contains(func.name)) {
      throw new UnsupportedOperationException(
        s"A function named ${func.name} is already registered"
      )
    }
    functions.put(func.name, func)
  }

  def listFunctions: Map[String, InterpretHFunction] = functions.toMap
}

object IntrinsicFunctionRegistry {
  def createStandard(
      marshallerRegistry: MarshallerRegistry
  ): IntrinsicFunctionRegistry = {
    val ret = new IntrinsicFunctionRegistry(marshallerRegistry)
    ret.register(marshallerRegistry.wrapFunction(Math.sin(_), "sin"))
    ret.register(marshallerRegistry.wrapFunction(Math.cos(_), "cos"))
    ret.register(marshallerRegistry.wrapFunction(Math.tan(_), "tan"))
    ret.register(marshallerRegistry.wrapFunction(Math.asin(_), "asin"))
    ret.register(marshallerRegistry.wrapFunction(Math.acos(_), "acos"))
    ret.register(marshallerRegistry.wrapFunction(Math.atan(_), "atan"))
    ret.register(marshallerRegistry.wrapFunction(Math.sinh(_), "sinh"))
    ret.register(marshallerRegistry.wrapFunction(Math.cosh(_), "cosh"))
    ret.register(marshallerRegistry.wrapFunction(Math.tanh(_), "tanh"))
    ret.register(
      marshallerRegistry.wrapFunction(
        { v: Either[Long, Double] =>
          v match {
            case Left(n)  => Left(Math.abs(n))
            case Right(x) => Right(Math.abs(x))
          }
        },
        "abs"
      )
    )
    ret.register(marshallerRegistry.wrapFunction(Math.sqrt(_), "sqrt"))
    ret.register(marshallerRegistry.wrapFunction(Math.exp(_), "exp"))
    ret.register(marshallerRegistry.wrapFunction(Math.log(_), "ln"))
    ret.register(marshallerRegistry.wrapFunction(Math.pow(_, _), "pow"))
    ret.register(
      marshallerRegistry.wrapFunction(
        (a: Either[String, ArrayBuffer[Value]], b: Int, c: Int) => {
          a.left.map(_.slice(b, c)).map(_.slice(b, c))
        },
        "slice"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction((v: Value) => v.deepCopy, "deepCopy")
    )
    ret.register(
      marshallerRegistry
        .wrapFunction((v: Value) => v.getType.toString, "typeof")
    )
    ret.register(
      marshallerRegistry
        .wrapFunction(
          (a: ArrayBuffer[Value], i: Int) => a.remove(i),
          "remove!"
        ) |
        marshallerRegistry.wrapFunction(
          (a: mutable.HashMap[String, Value], s: String) => a.remove(s),
          "remove!"
        )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (haystack: String, needle: String) => haystack startsWith needle,
        "startsWith?"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (haystack: String, needle: String) => haystack endsWith needle,
        "endsWith?"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (needle: String, haystack: String) => haystack contains needle,
        "substring?"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (haystack: String, needle: String) => haystack.indexOf(needle),
        "strFind"
      ) | marshallerRegistry.wrapFunction(
        (haystack: String, needle: String, start: Int) =>
          haystack.indexOf(needle, start),
        "strFind"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (haystack: String, needle: String) => haystack.lastIndexOf(needle),
        "strFindLast"
      ) | marshallerRegistry
        .wrapFunction(
          (haystack: String, needle: String, start: Int) =>
            haystack.lastIndexOf(needle, start),
          "strFindLast"
        )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (list: ArrayBuffer[Value], entry: Value) => list.indexOf(entry),
        "find"
      ) | marshallerRegistry.wrapFunction(
        (list: ArrayBuffer[Value], entry: Value, start: Int) =>
          list.indexOf(entry, start),
        "find"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (list: ArrayBuffer[Value], entry: Value) => list.lastIndexOf(entry),
        "findLast"
      ) | marshallerRegistry.wrapFunction(
        (list: ArrayBuffer[Value], entry: Value, start: Int) =>
          list.lastIndexOf(entry, start),
        "findLast"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (list: ArrayBuffer[Value], entry: Value) =>
          list.zipWithIndex
            .filter(_._1 == entry)
            .map(e => IntValue(e._2)): ArrayBuffer[Value],
        "findAll"
      ) | marshallerRegistry.wrapFunction(
        (map: mutable.HashMap[String, Value], entry: Value) =>
          ArrayBuffer
            .from(map.filter(_._2 == entry).keys.map(StringValue)): ArrayBuffer[
            Value // Blame scalafmt for this atrocity.
          ],
        "findAll"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (str: String, at: String) => ArraySeq.from(str.split(at)),
        "split"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunction(
        (strs: ArraySeq[String], delim: String) => strs.mkString(delim),
        "join"
      )
    )
    ret.register(
      marshallerRegistry.wrapFunctionEnvFallible(
        (env: Environment, name: String) => {
          env.trove.get[Map[String, Value]] match {
            case Some(pragmata) => Right(pragmata.getOrElse(name, NullValue))
            case None           => Left("no pragmata available – report to Hyphae!")
          }
        },
        "getPragma"
      )
    )
    ret
  }
}
