package flirora.hyphae.interpret

import java.io.{File => JFile}

import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.parse._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.{NoPosition, Position}
import java.{util => ju}

case class CallFrame(
    varStart: Int,
    functionName: String,
    lastPosition: Position,
    fileLocation: Option[JFile]
)

case class LoopFrame(discarded: Boolean = false) extends AnyVal {
  def discard: LoopFrame = copy(discarded = true)
}

class Environment private (
    // Holds the $-variables.
    val variables: ArrayBuffer[ValueRef],
    val file: Option[JFile],
    val callFrames: mutable.Stack[CallFrame],
    val loopFrames: mutable.Stack[LoopFrame],
    val intrinsicFunctions: Map[String, InterpretHFunction],
    val definedFunctions: Map[String, InterpretHFunction],
    val libraryFrames: mutable.Stack[Option[InterpretLibrary]],
    val trove: Trove[Id],
    val libraryCache: mutable.HashMap[String, InterpretLibrary],
    val definitionStore: ju.IdentityHashMap[
      InterpretLibrary,
      Map[String, InterpretHFunction]
    ]
) {

  private var discoveryError: ExecResult[Unit] = Success(())

  def this(
      dollar: ValueRef,
      baseSize: Int,
      file: Option[JFile],
      intrinsicFunctions: Map[String, InterpretHFunction],
      definedFunctions: Map[String, InterpretHFunction],
      libraryResolver: (String) => Either[String, InterpretLibrary],
      libraries: List[(String, String)]
  ) = {
    this(
      ArrayBuffer(dollar),
      file,
      mutable.Stack(CallFrame(0, "<main>", NoPosition, file)),
      mutable.Stack(),
      intrinsicFunctions,
      definedFunctions,
      mutable.Stack(None),
      new Trove,
      mutable.HashMap(),
      new ju.IdentityHashMap
    )
    this.pushScope(baseSize - 1)
    discoverFromMain(libraries, libraryResolver) match {
      case Success(m) => definitionStore.put(null, definedFunctions ++ m)
      case err        => discoveryError = err.map(_ => ())
    }
  }

  private def discoverFromMain(
      libraries: List[(String, String)],
      libraryResolver: (String) => Either[String, InterpretLibrary]
  ): ExecResult[Map[String, InterpretHFunction]] =
    for {
      dependencies <- Utils.mapOrFailResult(
        libraries,
        { e: (String, String) =>
          discoverModule(e._2, libraryResolver).map((e._1, _))
        }
      )
      importedFunctions = (for {
          (as, dependency) <- dependencies
          (funcName, func) <- dependency.definitions if func.exported
        } yield (
          s"$as\\$funcName",
          LibraryFunction[Int, ScopedBlock](dependency, func.func, funcName)
        )).toMap
    } yield importedFunctions

  private def discoverModule(
      libraryName: String,
      libraryResolver: (String) => Either[String, InterpretLibrary]
  ): ExecResult[InterpretLibrary] =
    libraryCache.get(libraryName) match {
      case Some(lib) => Success(lib) // Already discovered
      case None =>
        for {
          library <- libraryResolver(libraryName) match {
            case Right(m) => Success(m)
            case Left(err) =>
              Errored(RuntimeError(Context(this, NoPosition), err))
          }
          _ = libraryCache.put(libraryName, library)
          dependencies <- Utils.mapOrFailResult(
            library.imports,
            { e: (String, String) =>
              discoverModule(e._2, libraryResolver).map(lib => (e._1, lib))
            }
          )
          importedFunctions = (for {
              (as, dependency) <- dependencies
              (funcName, func) <- dependency.definitions if func.exported
            } yield (
              s"$as\\$funcName",
              LibraryFunction[Int, ScopedBlock](dependency, func.func, funcName)
            )).toMap
          _ = definitionStore.put(
            library,
            library.definitions.map({ case (k, v) => (k, v.func) }
            ) ++ importedFunctions
          )
        } yield library
    }

  def getRef(i: Int): ValueRef = {
    variables(callFrames.top.varStart + i)
  }
  def setRef(i: Int, x: ValueRef): Unit = {
    variables(callFrames.top.varStart + i) = x
  }

  def getVar(i: Int, ctx: Context): ExecResult[Value] = {
    getRef(i).get(ctx)
  }
  def setVar(i: Int, x: Value, ctx: Context): ExecResult[Unit] = {
    getRef(i).set(x, ctx)
  }

  def pushScope(n: Int): Unit = {
    val oldLength = variables.length
    variables.padToInPlace(variables.length + n, null)
    for (i <- oldLength until variables.length) {
      variables(i) = BoxedValue(NullValue)
    }
  }

  def popScope(n: Int): Unit = {
    variables.trimEnd(n)
  }

  def trace(expr: Value): Unit = println(expr.stringify)

  def pushCallFrame(
      functionName: String,
      lastPosition: Position,
      fileLocation: Option[JFile]
  ): Unit =
    callFrames.push(
      CallFrame(variables.length, functionName, lastPosition, fileLocation)
    )

  def popCallFrame(): Unit = callFrames.pop()

  def pushLoopFrame(): Unit = loopFrames.push(LoopFrame())

  def discardOnCurrentLoopFrame(ctx: Context): ExecResult[Unit] = {
    if (loopFrames.isEmpty)
      Errored(RuntimeError(ctx, "discard called outside of a loop"))
    else {
      loopFrames.push(loopFrames.pop().discard)
      Success(())
    }
  }

  def popLoopFrame(): LoopFrame = loopFrames.pop()

  def pushLibraryFrame(lib: Option[InterpretLibrary]) = libraryFrames.push(lib)

  def popLibraryFrame(): Unit = libraryFrames.pop()

  def getFunction(name: String, ctx: Context): ExecResult[InterpretHFunction] =
    getFunction(libraryFrames.top, name, ctx)

  def getFunction(
      library: Option[InterpretLibrary],
      name: String,
      ctx: Context
  ): ExecResult[InterpretHFunction] = {
    intrinsicFunctions.get(name) match {
      case Some(s) => return Success(s)
      case None    => ()
    }
    val store = definitionStore.get(library.orNull)
    if (store == null)
      return Errored(RuntimeError(ctx, s"no module under $library"))
    store.get(name) match {
      case Some(s) => Success(s)
      case None =>
        Errored(RuntimeError(ctx, s"no function named $name"))
    }
  }

  def getDiscoveryError: ExecResult[Unit] = discoveryError

  def getCurrentLibrary: Option[InterpretLibrary] = libraryFrames.top
}
