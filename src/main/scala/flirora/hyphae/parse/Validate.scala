package flirora.hyphae.parse

import java.io.{File => JFile}

import scala.util.parsing.input.Position

case class ValidateError(path: Option[JFile], pos: Position, message: String)
    extends CompilationError {
  override def show: String =
    s"Validation error at ${Utils.prettifyFilename(path)}:$pos: $message"
}

object Validate {
  def validate(
      expression: AstExpression
  )(implicit path: Option[JFile]): Seq[ValidateError] =
    expression match {
      case AstLiteral(_)             => Seq.empty
      case DotPath(ex, _)            => validate(ex)
      case IndexPath(ex, ix)         => validate(ex) ++ validate(ix)
      case OpInvoke(_, left, right)  => validate(left) ++ validate(right)
      case UnaryOpInvoke(_, ex)      => validate(ex)
      case ListConstructor(elements) => elements.flatMap(validate)
      case ObjectConstructor(elements) => {
        val duplicateKeys = elements.groupBy(_._1).filter(_._2.size > 1).keys
        val otherErrors = elements.map(_._2).flatMap(validate)
        if (duplicateKeys.nonEmpty) {
          ValidateError(
            path,
            expression.pos,
            s"Duplicate keys ${duplicateKeys.mkString(", ")}"
          ) +: otherErrors
        } else otherErrors
      }
      case Var(_)                => Seq.empty
      case FunctionCall(_, args) => args.flatMap(validate(_))
      case IndirectFunctionCall(f, args) =>
        validate(f) ++ args.flatMap(validate(_))
      case FunctionLiteral(name)                              => Seq.empty
      case LambdaExpression(parameters, body, captures, file) => validate(body)
    }

  def validate(
      store: AstStore
  )(implicit path: Option[JFile]): Seq[ValidateError] =
    store match {
      case Var(_)                => Seq.empty
      case StoreDotPath(_, _)    => Seq.empty
      case StoreIndexPath(_, ex) => validate(ex)
    }

  def isVarNameIllegal(name: String): Boolean = {
    Set("", "<", "!")(name)
  }

  def validate(
      statement: AstStatement
  )(implicit path: Option[JFile]): Seq[ValidateError] =
    statement match {
      case Declare(name, ex) if isVarNameIllegal(name) =>
        ValidateError(
          path,
          statement.pos,
          s"Cannot declare variable named $$$name"
        ) +: validate(ex)
      case Declare(_, ex)     => validate(ex)
      case Assign(s, ex)      => validate(s) ++ validate(ex)
      case OpAssign(s, ex, _) => validate(s) ++ validate(ex)
      case IfStatement(cond, block, block2) =>
        validate(cond) ++ validate(block) ++ validate(block2)
      case ForStatement(over, block, _, _) =>
        validate(over) ++ validate(block)
      case WithStatement(on, block, _) => validate(on) ++ validate(block)
      case TraceStatement(ex)          => validate(ex)
      case ReturnStatement(ex)         => validate(ex)
      case DiscardStatement()          => Seq()
    }

  def validate(
      rawBlock: RawBlock
  )(implicit path: Option[JFile]): Seq[ValidateError] = {
    rawBlock.statements.flatMap(validate(_))
  }

  def validate(
      program: AstProgram
  )(implicit path: Option[JFile]): Seq[ValidateError] = {
    validate(program.main) ++
      program.definedFunctions.values.flatMap(f => validate(f.body))
  }

  def validate(
      library: AstLibrary
  )(implicit path: Option[JFile]): Seq[ValidateError] = {
    Seq.from(library.definitions.values.flatMap(f => validate(f.func.body)))
  }
}
