package flirora.hyphae

package object parse {
  type AstExpression = Expression[String, RawBlock]
  type AstStore = Store[String, RawBlock]
  type AstStatement = Statement[String, RawBlock]
  type AstDefinedFunction = DefinedFunction[String, RawBlock]
  type AstProgram = Program[String, RawBlock]
  type AstLibrary = Library[String, RawBlock]
}
