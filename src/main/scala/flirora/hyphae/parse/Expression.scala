package flirora.hyphae.parse

import flirora.hyphae.interpret.{Environment, ExecResult, Success}
import flirora.hyphae.parse.ValueRef.Context

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.Positional
import flirora.hyphae.interpret.Errored
import flirora.hyphae.interpret.ScopedBlock
import java.io.{File => JFile}

sealed trait Expression[V, B] extends Positional {
  def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): Expression[V2, B2]

  def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value]
}

case class AstLiteral[V, B](value: Value) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): AstLiteral[V2, B2] =
    AstLiteral(value).setPos(pos)

  override def eval(environment: Environment)(implicit
      ev: V =:= Int,
      ev2: B =:= ScopedBlock
  ): ExecResult[Value] = Success(value)
}

case class DotPath[V, B](obj: Expression[V, B], path: Either[Int, String])
    extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): DotPath[V2, B2] =
    DotPath(obj.map(varTransform, blockTransform), path).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    for {
      o <- obj.eval(environment)
      result <- Utils.indexed(ctx, o, path)
    } yield result
  }
}
case class IndexPath[V, B](obj: Expression[V, B], path: Expression[V, B])
    extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): IndexPath[V2, B2] =
    IndexPath(
      obj.map(varTransform, blockTransform),
      path.map(varTransform, blockTransform)
    ).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    for {
      o <- obj.eval(environment)
      p <- path.eval(environment)
      result <- Utils.indexed(ctx, o, p)
    } yield result
  }
}

case class OpInvoke[V, B](
    op: Operator,
    left: Expression[V, B],
    right: Expression[V, B]
) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): OpInvoke[V2, B2] =
    OpInvoke(
      op,
      left.map(varTransform, blockTransform),
      right.map(varTransform, blockTransform)
    ).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    op match {
      case eager: EvaluableOperator =>
        for {
          a <- left.eval(environment)
          b <- right.eval(environment)
          result <- eager.evalAsResult(a, b, ctx)
        } yield result
      case Operator.LogicalAnd =>
        left
          .eval(environment)
          .flatMap(Utils.tryBool(ctx, _)) match {
          case Success(true)  => right.eval(environment)
          case Success(false) => Success(BoolValue(false))
          case other          => other.map(_ => NullValue)
        }
      case Operator.LogicalOr =>
        left
          .eval(environment)
          .flatMap(Utils.tryBool(ctx, _)) match {
          case Success(false) => right.eval(environment)
          case Success(true)  => Success(BoolValue(true))
          case other          => other.map(_ => NullValue)
        }
    }
  }
}

case class UnaryOpInvoke[V, B](op: PrefixOp, expr: Expression[V, B])
    extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): UnaryOpInvoke[V2, B2] =
    UnaryOpInvoke(op, expr.map(varTransform, blockTransform)).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    expr
      .eval(environment)
      .flatMap(v => op.evalAsResult(v, ctx))
  }
}

case class ListConstructor[V, B](elements: ArraySeq[Expression[V, B]])
    extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): ListConstructor[V2, B2] =
    ListConstructor(elements.map(_.map(varTransform, blockTransform)))
      .setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] =
    Utils
      .mapOrFailResult(
        elements,
        { e: Expression[V, B] =>
          e.eval(environment)
        }
      )
      .map(e => ListValue(ArrayBuffer.from(e)))
}

case class ObjectConstructor[V, B](
    elements: ArraySeq[(String, Expression[V, B])]
) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): ObjectConstructor[V2, B2] =
    ObjectConstructor(elements.map({
      case (k, v) => (k, v.map(varTransform, blockTransform))
    })).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] =
    Utils
      .mapOrFailResult(
        elements,
        { p: (String, Expression[V, B]) =>
          p._2.eval(environment).map(p._1 -> _)
        }
      )
      .map(e => ObjectValue(mutable.HashMap.from(e)))
}

case class FunctionCall[V, B](name: String, args: ArraySeq[Expression[V, B]])
    extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): FunctionCall[V2, B2] =
    FunctionCall(name, args.map(_.map(varTransform, blockTransform)))
      .setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    for {
      func <- environment.getFunction(name, ctx)
      argsRes <- Utils.mapOrFailResult(
        args,
        (e: Expression[V, B]) => e.eval(environment)
      )
      result <- func.eval(environment, argsRes, pos)
    } yield result
  }
}

case class IndirectFunctionCall[V, B](
    functionHandle: Expression[V, B],
    args: ArraySeq[Expression[V, B]]
) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): IndirectFunctionCall[V2, B2] =
    IndirectFunctionCall(
      functionHandle.map(varTransform, blockTransform),
      args.map(_.map(varTransform, blockTransform))
    )
      .setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    for {
      func1 <- functionHandle.eval(environment)
      func <- func1 match {
        case f: FunctionValue => Success(f)
        case _ =>
          Errored(
            RuntimeError(
              ctx,
              s"${func1.stringify} cannot be called as a function"
            )
          )
      }
      argsRes <- Utils.mapOrFailResult(
        args,
        (e: Expression[V, B]) => e.eval(environment)
      )
      result <- func.eval(environment, argsRes, pos)
    } yield result
  }
}

case class FunctionLiteral[V, B](name: String) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): FunctionLiteral[V2, B2] = FunctionLiteral(name).setPos(pos)

  override def eval(environment: Environment)(implicit
      ev: V =:= Int,
      ev2: B =:= ScopedBlock
  ): ExecResult[Value] = {
    val ctx = Context(environment, pos)
    for {
      func <- environment.getFunction(name, ctx)
    } yield FunctionHandle(func, environment.getCurrentLibrary)
  }
}

case class LambdaExpression[V, B](
    parameters: ArraySeq[V],
    body: B,
    captures: ArraySeq[Store[V, B]],
    file: Option[JFile]
) extends Expression[V, B] {
  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): Expression[V2, B2] =
    LambdaExpression(
      parameters.map(varTransform),
      blockTransform(body),
      captures.map(_.map(varTransform, blockTransform)),
      file
    ).setPos(pos)

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] = {
    for {
      captureValues1 <- Utils.mapOrFailResult(
        captures,
        (e: Store[V, B]) => e.evalAsStore(environment)
      )
      captureValues = ArraySeq.from(captureValues1)
    } yield LambdaValue(
      captureValues,
      parameters.size,
      body,
      file,
      pos,
      environment.getCurrentLibrary
    )
  }
}

sealed trait Store[V, B] extends Positional {
  def deref: Expression[V, B]
  def map[V2, B2](varTransform: V => V2, blockTransform: B => B2): Store[V2, B2]

  def evalAsStore(environment: Environment)(implicit
      ev: V =:= Int,
      ev2: B =:= ScopedBlock
  ): ExecResult[ValueRef]
}

case class Var[V, B](name: V) extends Store[V, B] with Expression[V, B] {
  override def deref: Expression[V, B] = this

  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): Var[V2, B2] = Var(varTransform(name)).setPos(pos)

  override def evalAsStore(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[ValueRef] =
    Success(environment.getRef(name))

  override def eval(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[Value] =
    environment.getVar(name, ValueRef.Context(environment, pos))
}

object Var {
  // Helper method to desugar variables
  def fromName(name: String): AstStore =
    if (name startsWith "$") Var(name.substring(1))
    else StoreDotPath(Var(""), Right(name))
}

case class StoreDotPath[V, B](obj: Store[V, B], path: Either[Int, String])
    extends Store[V, B] {
  override def deref: Expression[V, B] = DotPath[V, B](obj.deref, path)

  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): StoreDotPath[V2, B2] =
    StoreDotPath(obj.map(varTransform, blockTransform), path).setPos(pos)

  override def evalAsStore(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[ValueRef] = {
    val ctx = Context(environment, pos)
    for {
      o <- obj.deref.eval(environment)
      result <- Utils.indexedRef(ctx, o, path)
    } yield result
  }
}

case class StoreIndexPath[V, B](obj: Store[V, B], path: Expression[V, B])
    extends Store[V, B] {
  override def deref: Expression[V, B] = IndexPath[V, B](obj.deref, path)

  override def map[V2, B2](
      varTransform: V => V2,
      blockTransform: B => B2
  ): StoreIndexPath[V2, B2] =
    StoreIndexPath(
      obj.map(varTransform, blockTransform),
      path.map(varTransform, blockTransform)
    ).setPos(pos)

  override def evalAsStore(
      environment: Environment
  )(implicit ev: V =:= Int, ev2: B =:= ScopedBlock): ExecResult[ValueRef] = {
    val ctx = Context(environment, pos)
    for {
      o <- obj.deref.eval(environment)
      p <- path.eval(environment)
      result <- Utils.indexedRef(ctx, o, p)
    } yield result
  }
}
