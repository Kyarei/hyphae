package flirora.hyphae.parse

import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.interpret.{
  Environment,
  Errored,
  ExecResult,
  InterpretHFunction,
  InterpretLibrary,
  ScopedBlock,
  Success
}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.Position
import java.io.{File => JFile}
import scala.collection.immutable.ArraySeq
import flirora.hyphae.interpret.Returned

sealed trait Value {
  def stringify: String
  def getType: Type
  def deepCopy: Value = this
}

sealed abstract class NumValue extends Value {
  def asDouble: Double
}

object NumValue {
  def unapply(numValue: NumValue): Option[Double] = Some(numValue.asDouble)
}

case class RealValue(x: Double) extends NumValue {
  override def stringify: String = x.toString

  override def asDouble: Double = x

  override def getType: Type = RealType
}

case class IntValue(n: Long) extends NumValue {
  override def stringify: String = n.toString

  override def asDouble: Double = n.toDouble

  override def getType: Type = IntType
}

case class BoolValue(b: Boolean) extends Value {
  override def stringify: String = b.toString

  override def getType: Type = BoolType
}

case class StringValue(s: String) extends Value {
  override def stringify: String = s

  override def getType: Type = StringType
}

case class ListValue(list: ArrayBuffer[Value]) extends Value {
  def broadcast(other: ListValue, func: (Value, Value) => Value): ListValue = {
    ListValue(list.zip(other.list).map(func.tupled))
  }

  def broadcastLeft[A](left: A, func: (A, Value) => Value): ListValue = {
    ListValue(list.map(func(left, _)))
  }

  def broadcastRight[A](right: A, func: (Value, A) => Value): ListValue = {
    ListValue(list.map(func(_, right)))
  }

  def broadcast(
      other: ListValue,
      func: (Value, Value) => Either[String, Value]
  ): Either[String, ListValue] = {
    Utils
      .mapOrFail(list.zip(other.list), func.tupled)
      .map(a => ListValue(ArrayBuffer.from(a)))
  }

  def broadcastLeft[A](
      left: A,
      func: (A, Value) => Either[String, Value]
  ): Either[String, ListValue] = {
    Utils
      .mapOrFail(list, func(left, _))
      .map(a => ListValue(ArrayBuffer.from(a)))
  }

  def broadcastRight[A](
      right: A,
      func: (Value, A) => Either[String, Value]
  ): Either[String, ListValue] = {
    Utils
      .mapOrFail(list, func(_, right))
      .map(a => ListValue(ArrayBuffer.from(a)))
  }

  override def stringify: String = s"[${list.mkString(", ")}]"

  override def getType: Type = ListType

  override def deepCopy: Value = ListValue(list.map(_.deepCopy))
}

case class ObjectValue(obj: mutable.HashMap[String, Value]) extends Value {
  override def stringify: String = s"{$stringifyBody}"

  private def stringifyBody: String = {
    obj
      .map({ case (key, value) => s"${key}: ${value.stringify}" })
      .mkString(", ")
  }

  override def getType: Type = ObjectType

  override def deepCopy: Value =
    ObjectValue(obj.map({
      case (k, v) => k -> v.deepCopy
    }))
}

case object NullValue extends Value {
  override def stringify: String = "null"

  override def getType: Type = NullType
}

trait FunctionValue extends Value {
  override def getType: Type = FunctionType

  def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  ): ExecResult[Value]
}

case class FunctionHandle(
    func: InterpretHFunction,
    enclosingLibrary: Option[InterpretLibrary]
) extends FunctionValue {
  override def stringify: String = s"<function ${func.name}>"

  override def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  ): ExecResult[Value] = {
    environment.pushLibraryFrame(enclosingLibrary)
    val res = func.eval(environment, args, callPos)
    environment.popLibraryFrame()
    res
  }
}

/*
 * Lambdas are called with the following conventions:
 * captures take var slots from 0, followed by parameters
 */
case class LambdaValue(
    captures: ArraySeq[ValueRef],
    nParams: Int,
    body: ScopedBlock,
    fileLocation: Option[JFile],
    position: Position,
    enclosingLibrary: Option[InterpretLibrary]
) extends FunctionValue {
  override def stringify: String = "<anonymous function>"

  override def eval(
      environment: Environment,
      args: Iterable[Value],
      callPos: Position
  ): ExecResult[Value] = {
    val ctx = Context(environment, position)
    environment.pushLibraryFrame(enclosingLibrary)
    environment.pushCallFrame("<lambda>", callPos, fileLocation)
    environment.pushScope(body.localSize)
    val res = for {
      m <-
        Utils
          .mapOrFailResult(
            (0 until nParams).zip(args),
            { p: (Int, Value) =>
              environment.setVar(captures.size + p._1, p._2, ctx)
            }
          )
          .map(_ => NullValue)
      _ = for ((capture, i) <- captures.zipWithIndex) {
        environment.setRef(i, capture)
      }
      result <- body.exec(environment) match {
        case Returned(x) => Success(x)
        case e           => e.map(_ => NullValue)
      }
    } yield result
    environment.popScope(body.localSize)
    environment.popCallFrame()
    environment.popLibraryFrame()
    res
  }
}

trait ValueRef {
  def get(ctx: Context): ExecResult[Value]
  def set(v: Value, ctx: Context): ExecResult[Unit]
}

object ValueRef {
  case class Context(environment: Environment, pos: Position)
}

case class BoxedValue(var value: Value) extends ValueRef {
  // The value inside is always available
  override def get(ctx: Context): ExecResult[Value] = Success(value)

  override def set(v: Value, ctx: Context): ExecResult[Unit] = {
    value = v
    Success(())
  }
}

case class IndexedValueRef(list: ListValue, index: Int) extends ValueRef {
  override def get(ctx: Context): ExecResult[Value] =
    if (index >= 0 && index < list.list.size) Success(list.list(index))
    else
      Errored(
        RuntimeError(ctx, s"Index out of bounds: $index > ${list.list.length}")
      )

  override def set(v: Value, ctx: Context): ExecResult[Unit] =
    if (index >= 0 && index < list.list.size) Success(list.list(index) = v)
    else
      Errored(
        RuntimeError(ctx, s"Index out of bounds: $index > ${list.list.length}")
      )
}

case class KeyedValueRef(obj: ObjectValue, key: String) extends ValueRef {
  override def get(ctx: Context): ExecResult[Value] =
    Success(obj.obj.getOrElse(key, NullValue))

  override def set(v: Value, ctx: Context): ExecResult[Unit] =
    Success(obj.obj.put(key, v))
}
