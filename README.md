## hyphae

An experimental JSON patching language.

### FAQ

**Why Scala?** I wanted to use a JVM language that didn't make this sort of thing painful to do.

**Why the JVM?** Because this was made for Minecraft (Java Edition).

**Why use GSON for JSON stuff?** Because this was made for Minecraft modding, and Minecraft likes to use GSON for some reason. Of course, if you'd like to use another JSON library, then you can write your own transcoder.

### The Language

A program consists of a number of *statements*. *Expressions* are a different thing from statements.

Operations are taken on an input JSON element, which can be referred to by `$`. Literals for JSON values are written in roughly the same way as in JSON, except that object keys don't need to be quoted and trailing commas are allowed.

To get a part of an element:

```
$.foo              # get the part of $ under the "foo" key
$["foo"]           # same
foo                # shorthand
$["+"]             # + can't be used in an identifier
$[3]               # get element number 3 of $
$.3                # same
```

You can assign to storage locations using the `=` operator. Here are some other operators:

```
# arithmetic operators; / does integer division on integers
$a + $b
$a - $b
$a * $b
$a / $b
$a % $b
-$a
# coerce into number if possible
+$a
# string or list concatenation; object merging; stringifies one
# or both arguments if it doesn't know what to do
$a ~ $b
# append $b to $a
$a :~ $b
# explicitly coerce into string
~$a
# length of string or list
%$a
# comparisons
$a == $b
$a != $b
$a < $b
$a > $b
$a <= $b
$a >= $b
# logical
$a && $b
$a || $b
!$a
```

The binary operators (other than comparison or logical operators) have compound forms such as `+=`.

The `trace`-statement prints out a value for debugging:

```
trace "Hello, world!"
```

The `if`-statement is written as follows:

```
if hardness < 3.0 {
  # code to execute if $.hardness is less than 3
} else {
  # code to execute otherwise
}
```

Of course, the `else` branch is optional, but the braces are necessary.

The `for`-statement iterates over a list or object. Inside the body, `$` takes on the value of the current element, and a new variable, `$<`, is introduced to hold the index or the key:

```
for thing {
  trace $< ~ " maps to " ~ $
}
```

On `{"thing": [6, 3, 7]}`, it prints

```
0 maps to 6
1 maps to 3
2 maps to 7
```

On `{"thing": {"cow": "buwch", "pig": "mochyn", "snake": "neidr"}}`, it prints

```
cow maps to buwch
pig maps to mochyn
snake maps to neidr
```

The `discard` statement is used inside a `for`-statement to schedule an element to be removed. (Note that the actual removal happens at the end of the loop.)

```
for $ {
  if $ < 0 {
    discard
  }
}
```

maps `[5, 2, -9, 7, -3]` to `[5, 2, 7]`.

The `with` statement binds `$` to an expression within a block:

```
trace $
with foo {
  trace $
}
```

outputs for `{"foo": "bar"}`

```
{foo: bar}
bar
```

You can declare variables with `let`. Such variables must be prepended with `$`:

```
let $myString = "hello everyone!"
trace $myString
```

Finally, you can declare functions:

```
def add($a, $b) {
  return = $a + $b # notice the equals sign
}
def funny?($n) {
  return = $n == 69 || $n == 420
}
trace funny?(add(33, 36))
```

You can even store functions in variables!

```
let $a = def[abs]
let $b = ($x) -> { return = $x + 1 }
trace $b($a(-3)) # => 4
```

This is an experimental feature that was a pain in the butt to get working right. Try it and report any bugs!

#### Libraries

Hyphae now actually supports libraries. Libraries can contain only function definitions, optionally with the `export` keyword to make them visible to users. Example:

```
def secret() {
  return = "himitsu"
}

export def cleaned() {
  return = "*" * %secret()
}
```

To use a library, use the `import` statement:

```
import myLibrary = "path/to/module.hy"

# Now you can use myLibrary\cleaned
# the `secret` function defined in the library is not visible here
```

What in particular goes on the right side of the import statement depends on the application you're writing for.

#### Pragmata

Pragmata allow you to define values that are visible to the application embedding Hyphae (in order to, for instance, provide metadata).

```
pragma priority = 5 # Define a pragma
pragma funny?       # Shortcut for defining as true
```

Currently, only integer, real, boolean, and string values are supported.

### Standard functions

* `sin`, `cos`, `tan`, `asin`, `acos`, `atan`, `sinh`, `cosh`, `tanh`, `abs`, `sqrt`, `exp`, `ln`, `pow`
* `slice($stringOrList, $start, $end)`
* `deepCopy($thing)`
* `typeof(thing)`
* `remove!($list, $index)` or `remove!($object, $key)` – returns the removed value
* `startsWith?($haystack, $needle)` and `endsWith($haystack, $needle)`
* `substring?($needle, $haystack)`
* `strFind($haystack, $needle[, $start])` and `strFindLast(...)`
* `find($list, $entry[, $start])` and `findLast(...)`
* `findAll($list, $entry)` and `findAll($map, $value)` – returns a list of indices or keys
* `split($str, $delim)` and `join($strs, $delim)`
* `getPragma($name)`

### Embedding into your program

Hope you know Scala if you want to do this.

The whole deal goes as follows:

* Create a `MarshallerRegistry` object using `MarshallerRegistry.createStandard`, then register any custom `Transcoder`s you want to register to use for inputs and outputs to your custom functions (probably with `registerMarshallerWithOptional` to automatically register transcoders for `Option[T]`)
* Create an `IntrinsicFunctionRegistry` object using `IntrinsicFunctionRegistry.createStandard`, then register any custom functions
* Parse some source code using `Hyphae.parse`
* Run the program on your input using `Hyphae.run` or more likely `Hyphae.runOverTranscoded`, which transcodes the input and output elements with a `Transcoder`

For creating intrinsic functions, the `MarshallerRegistry#wrapFunction` and `wrapFunctionFallible` methods are provided for wrapping Scala functions to be used by Hyphae. However, make sure to catch any exceptions that might be thrown (you can catch the exceptions and return an `ExecResult[TheReturnType]` instead).

