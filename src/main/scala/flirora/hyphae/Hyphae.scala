package flirora.hyphae

import java.io.{File => JFile}

import flirora.hyphae.interpret._
import flirora.hyphae.interpret.interop.IntrinsicFunctionRegistry
import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.parse._
import flirora.hyphae.transcode.Transcoder

import scala.collection.mutable.ArrayBuffer
import scala.util.parsing.input.NoPosition

object Hyphae {
  def parse(
      code: String,
      path: Option[JFile]
  ): Either[Iterable[InterpreterError], InterpretProgram] = {
    val ast = HyphaeParser.parseProgram(path, code) match {
      case Left(err)  => return Left(Seq(err))
      case Right(ast) => ast
    }
    val validationErrors =
      ArrayBuffer.from[CompilationError](Validate.validate(ast)(path))
    val resolved = AssignVariables.transform(ast, path, validationErrors)
    if (validationErrors.isEmpty) Right(resolved) else Left(validationErrors)
  }

  def parseLibrary(
      code: String,
      path: Option[JFile]
  ): Either[Iterable[InterpreterError], InterpretLibrary] = {
    val ast = HyphaeParser.parseLibrary(path, code) match {
      case Left(err)  => return Left(Seq(err))
      case Right(ast) => ast
    }
    val validationErrors =
      ArrayBuffer.from[CompilationError](Validate.validate(ast)(path))
    val resolved = AssignVariables.transform(ast, path, validationErrors)
    if (validationErrors.isEmpty) Right(resolved) else Left(validationErrors)
  }

  def run(
      program: InterpretProgram,
      root: Value,
      intrinsicFunctions: IntrinsicFunctionRegistry,
      path: Option[JFile],
      libraryResolver: (String) => Either[String, InterpretLibrary],
      troveCallback: Trove[Id] => Unit = _ => ()
  ): Either[InterpreterError, Value] = {
    val boxedValue = BoxedValue(root)
    val shadowedFunctionNames =
      intrinsicFunctions.listFunctions.keySet & program.definedFunctions.keySet
    val environment =
      new Environment(
        boxedValue,
        program.main.localSize,
        path,
        intrinsicFunctions.listFunctions,
        program.definedFunctions,
        libraryResolver,
        program.imports
      )
    environment.getDiscoveryError match {
      case Errored(e) => return Left(e)
      case _          => ()
    }
    environment.trove.put(program.pragmata, allowOverwrites = false)
    troveCallback(environment.trove)
    if (shadowedFunctionNames.nonEmpty) {
      return Left(
        RuntimeError(
          Context(environment, NoPosition),
          s"built-in functions ${shadowedFunctionNames.mkString(", ")} are shadowed by user-defined functions"
        )
      )
    }
    program.main
      .exec(environment)
      .getResult(boxedValue.get(Context(environment, NoPosition)) match {
        case Success(t) => t
        case Errored(e) => return Left(e)
        case x =>
          return Left(
            RuntimeError(
              Context(environment, NoPosition),
              s"impossible: boxed value yields $x?"
            )
          )
      })
  }

  def runOverTranscoded[T](
      transcoder: Transcoder[T],
      program: InterpretProgram,
      root: T,
      intrinsicFunctions: IntrinsicFunctionRegistry,
      path: Option[JFile],
      libraryResolver: (String) => Either[String, InterpretLibrary],
      troveCallback: Trove[Id] => Unit = _ => ()
  ): Either[InterpreterError, T] = {
    for {
      value <- transcoder.toValue(root)
      newValue <- run(
        program,
        value,
        intrinsicFunctions,
        path,
        libraryResolver,
        troveCallback
      )
      result <- transcoder.fromValue(newValue)
    } yield result
  }
}
