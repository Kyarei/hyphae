package flirora.hyphae.interpret.interop

import flirora.hyphae.interpret.Trove
import flirora.hyphae.transcode.PrimitiveTranscoders._
import flirora.hyphae.transcode.Transcoder

import scala.reflect.runtime.{universe => ru}

class MarshallerRegistry extends AbstractMarshallerRegistry {
  private val trove: Trove[Transcoder] = new Trove

  def marshallerFor[T: ru.TypeTag]: Transcoder[T] = {
    trove
      .get[T]
      .getOrElse(throw new NoSuchElementException(s"${ru.typeTag[T]}"))
  }
  def registerMarshaller[T: ru.TypeTag](marshaller: Transcoder[T]): Unit = {
    trove.put[T](marshaller, allowOverwrites = false)
  }
}

object MarshallerRegistry {
  case class TypeKey(tag: ru.TypeTag[_]) {
    override def canEqual(that: Any): Boolean = that.isInstanceOf[TypeKey]

    override def hashCode(): Int = tag.hashCode()
  }

  def createStandard: MarshallerRegistry = {
    val ret = new MarshallerRegistry
    ret.registerMarshallerWithOptional(IdTranscoder)
    ret.registerMarshaller(UnitTranscoder)
    ret.registerMarshallerWithHomListAndOptional(ByteTranscoder)
    ret.registerMarshallerWithHomListAndOptional(ShortTranscoder)
    ret.registerMarshallerWithHomListAndOptional(IntTranscoder)
    ret.registerMarshallerWithHomListAndOptional(LongTranscoder)
    ret.registerMarshallerWithHomListAndOptional(FloatTranscoder)
    ret.registerMarshallerWithHomListAndOptional(DoubleTranscoder)
    ret.registerMarshallerWithOptional(LongOrDoubleTranscoder)
    ret.registerMarshallerWithHomListAndOptional(BooleanTranscoder)
    ret.registerMarshallerWithHomListAndOptional(StringTranscoder)
    ret.registerMarshallerWithOptional(HetListTranscoder)
    ret.registerMarshallerWithOptional(HetArrayBufferTranscoder)
    ret.registerMarshallerWithOptional(MutableHashMapTranscoder)
    ret.registerMarshallerWithOptional(SliceableTranscoder)
    ret
  }
}
