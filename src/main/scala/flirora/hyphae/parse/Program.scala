package flirora.hyphae.parse

case class Program[V, B](
    main: B,
    definedFunctions: Map[String, DefinedFunction[V, B]],
    pragmata: Map[String, Value],
    imports: List[(String, String)]
) {}
