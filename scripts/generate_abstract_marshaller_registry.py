# Curse you, Scala, for not supporting variadic generics

header = """/*
  This code has been generated by scripts/abstract_marshaller_registry.py;
  edit that file instead of this one.
*/

package flirora.hyphae.interpret.interop

import flirora.hyphae.interpret.{
  Environment,
  Errored,
  ExecResult,
  ScopedBlock,
  Success
}
import flirora.hyphae.parse.NullValue
import flirora.hyphae.parse.ValueRef.Context
import flirora.hyphae.parse.{RuntimeError, Utils, Value}
import flirora.hyphae.transcode.{Transcoder, TranscodingError}
import flirora.hyphae.transcode.PrimitiveTranscoders.{HomArrayTranscoder, OptionalTranscoder}

import scala.reflect.ClassTag
import scala.reflect.runtime.{universe => ru}
import scala.util.parsing.input.{Position, NoPosition}

trait AbstractMarshallerRegistry {
  def marshallerFor[T: ru.TypeTag]: Transcoder[T]
  def registerMarshaller[T: ru.TypeTag](marshaller: Transcoder[T]): Unit
  def registerMarshallerWithOptional[T: ru.TypeTag](marshaller: Transcoder[T]): Unit = {
    registerMarshaller(marshaller)
    registerMarshaller(OptionalTranscoder(marshaller))
  }
  def registerMarshallerWithHomListAndOptional[T: ru.TypeTag](marshaller: Transcoder[T]): Unit = {
    registerMarshallerWithOptional(marshaller)
    registerMarshallerWithOptional(HomArrayTranscoder(marshaller)(Utils.typeToClassTag[T]))
    registerMarshallerWithOptional(HomArrayTranscoder(OptionalTranscoder(marshaller))(Utils.typeToClassTag[Option[T]]))
  }
"""


def body(arity: int):
    atparams = [f"A{i}" for i in range(arity)]
    aparams = [f"a{i}" for i in range(arity)]
    tp_str = ", ".join(atparams)
    tp_str_c = "".join(", " + p for p in atparams)
    in_bracket = "".join(f"{p}: ru.TypeTag, " for p in atparams)
    marshaller_decls = f"\n{' ' * 6}".join(
        f"private val marshaller{p} = registry.marshallerFor[{p}]"
        for p in atparams)
    vp_str = ", ".join(f"v{p}" for p in aparams)
    tvp_str = ", ".join(f"t{p}" for p in aparams)
    tvp_str_c = "".join(f", t{p}" for p in aparams)
    value_extractor = f"\n{' ' * 14}".join(
        f"ta{i} <- marshallerA{i}.fromValue(va{i})"
        for i in range(arity))
    signature_params = ", ".join(
        f"${{marshaller{p}.typename}}" for p in atparams)
    signature = f"s\"({signature_params}) -> ${{marshallerB.typename}}\""
    return f"""def wrapFunction[{in_bracket}B: ru.TypeTag](
    func: ({tp_str}) => B,
    _name: String
  ): IntrinsicFunction = {{
    val registry = this
    new IntrinsicFunction {{
      {marshaller_decls}
      private val marshallerB = registry.marshallerFor[B]

      override val name: String = _name

      override def evalOrTypeError(environment: Environment, args: Iterable[Value], callPos: Position)(
        implicit ev: Int =:= Int,
        ev2: ScopedBlock =:= ScopedBlock
      ): Either[TranscodingError, ExecResult[Value]] = {{
        environment.pushCallFrame(name, callPos, None)
        val res = args match {{
          case Seq({vp_str}) =>
            for {{
              {value_extractor}
              vb0 <- marshallerB.toValue(func({tvp_str}))
            }} yield Success(vb0)
          case _ =>
            Left(TranscodingError(s\"Wrong number of arguments: expected {arity} but got ${{args.size}}\"))
        }}
        environment.popCallFrame()
        res
      }}

      override val signature: String = {signature}
    }}
  }}

  def wrapFunctionFallible[{in_bracket}B: ru.TypeTag](
    func: ({tp_str}) => Either[String, B],
    _name: String
  ): IntrinsicFunction = {{
    val registry = this
    new IntrinsicFunction {{
      {marshaller_decls}
      private val marshallerB = registry.marshallerFor[B]

      override val name: String = _name

      override def evalOrTypeError(environment: Environment, args: Iterable[Value], callPos: Position)(
        implicit ev: Int =:= Int,
        ev2: ScopedBlock =:= ScopedBlock
      ): Either[TranscodingError, ExecResult[Value]] = {{
        val ctx = Context(environment, NoPosition)
        environment.pushCallFrame(name, callPos, None)
        val res = args match {{
          case Seq({vp_str}) =>
            for {{
              {value_extractor}
              tb0 <- Right(func({tvp_str}) match {{
                case Left(err) => Errored(RuntimeError(ctx, err))
                case Right(v)  => Success(v)
              }})
              vb0 <- tb0 match {{
                case Success(s) => marshallerB.toValue(s).map(Success(_))
                case fail       => Right(fail.map(_ => NullValue))
              }}
            }} yield vb0
          case _ =>
            Left(TranscodingError(s\"Wrong number of arguments: expected {arity} but got ${{args.size}}\"))
        }}
        environment.popCallFrame()
        res
      }}

      override val signature: String = {signature}
    }}
  }}

  def wrapFunctionEnv[{in_bracket}B: ru.TypeTag](
    func: (Environment{tp_str_c}) => B,
    _name: String
  ): IntrinsicFunction = {{
    val registry = this
    new IntrinsicFunction {{
      {marshaller_decls}
      private val marshallerB = registry.marshallerFor[B]

      override val name: String = _name

      override def evalOrTypeError(environment: Environment, args: Iterable[Value], callPos: Position)(
        implicit ev: Int =:= Int,
        ev2: ScopedBlock =:= ScopedBlock
      ): Either[TranscodingError, ExecResult[Value]] = {{
        environment.pushCallFrame(name, callPos, None)
        val res = args match {{
          case Seq({vp_str}) =>
            for {{
              {value_extractor}
              vb0 <- marshallerB.toValue(func(environment{tvp_str_c}))
            }} yield Success(vb0)
          case _ =>
            Left(TranscodingError(s\"Wrong number of arguments: expected {arity} but got ${{args.size}}\"))
        }}
        environment.popCallFrame()
        res
      }}

      override val signature: String = {signature}
    }}
  }}

  def wrapFunctionEnvFallible[{in_bracket}B: ru.TypeTag](
    func: (Environment{tp_str_c}) => Either[String, B],
    _name: String
  ): IntrinsicFunction = {{
    val registry = this
    new IntrinsicFunction {{
      {marshaller_decls}
      private val marshallerB = registry.marshallerFor[B]

      override val name: String = _name

      override def evalOrTypeError(environment: Environment, args: Iterable[Value], callPos: Position)(
        implicit ev: Int =:= Int,
        ev2: ScopedBlock =:= ScopedBlock
      ): Either[TranscodingError, ExecResult[Value]] = {{
        val ctx = Context(environment, NoPosition)
        environment.pushCallFrame(name, callPos, None)
        val res = args match {{
          case Seq({vp_str}) =>
            for {{
              {value_extractor}
              tb0 <- Right(func(environment{tvp_str_c}) match {{
                case Left(err) => Errored(RuntimeError(ctx, err))
                case Right(v)  => Success(v)
              }})
              vb0 <- tb0 match {{
                case Success(s) => marshallerB.toValue(s).map(Success(_))
                case fail       => Right(fail.map(_ => NullValue))
              }}
            }} yield vb0
          case _ =>
            Left(TranscodingError(s\"Wrong number of arguments: expected {arity} but got ${{args.size}}\"))
        }}
        environment.popCallFrame()
        res
      }}

      override val signature: String = {signature}
    }}
  }}
    """


footer = """}
"""

print(header)
for i in range(8):
    print(body(i))
print(footer)
