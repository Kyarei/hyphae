package flirora.hyphae.transcode
import flirora.hyphae.parse._

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

object PrimitiveTranscoders {
  case object IdTranscoder extends Transcoder[Value] {
    override def toValue(obj: Value): Either[TranscodingError, Value] =
      Right(obj)

    override def fromValue(value: Value): Either[TranscodingError, Value] =
      Right(value)

    override val typename: String = "any"
  }
  case object UnitTranscoder extends Transcoder[Unit] {
    override def toValue(obj: Unit): Either[TranscodingError, Value] =
      Right(NullValue)

    override def fromValue(value: Value): Either[TranscodingError, Unit] =
      Right(())

    override val typename: String = "null"
  }
  case object ByteTranscoder extends Transcoder[Byte] {
    override def toValue(obj: Byte): Either[TranscodingError, Value] =
      Right(IntValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Byte] =
      value match {
        case IntValue(n) if n >= Byte.MinValue && n <= Byte.MaxValue =>
          Right(n.toByte)
        case IntValue(n) =>
          Left(TranscodingError(s"integer $n doesn't fit in 8 bits"))
        case _ =>
          Left(TranscodingError(s"${value.getType} cannot be converted to int"))
      }

    override val typename: String = "int8"
  }
  case object ShortTranscoder extends Transcoder[Short] {
    override def toValue(obj: Short): Either[TranscodingError, Value] =
      Right(IntValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Short] =
      value match {
        case IntValue(n) if n >= Short.MinValue && n <= Short.MaxValue =>
          Right(n.toShort)
        case IntValue(n) =>
          Left(TranscodingError(s"integer $n doesn't fit in 16 bits"))
        case _ =>
          Left(TranscodingError(s"${value.getType} cannot be converted to int"))
      }

    override val typename: String = "int16"
  }
  case object IntTranscoder extends Transcoder[Int] {
    override def toValue(obj: Int): Either[TranscodingError, Value] =
      Right(IntValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Int] =
      value match {
        case IntValue(n) if n >= Int.MinValue && n <= Int.MaxValue =>
          Right(n.toInt)
        case IntValue(n) =>
          Left(TranscodingError(s"integer $n doesn't fit in 32 bits"))
        case _ =>
          Left(TranscodingError(s"${value.getType} cannot be converted to int"))
      }

    override val typename: String = "int32"
  }
  case object LongTranscoder extends Transcoder[Long] {
    override def toValue(obj: Long): Either[TranscodingError, Value] =
      Right(IntValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Long] =
      value match {
        case IntValue(n) => Right(n)
        case _ =>
          Left(TranscodingError(s"${value.getType} cannot be converted to int"))
      }

    override val typename: String = "int64"
  }
  case object FloatTranscoder extends Transcoder[Float] {
    override def toValue(obj: Float): Either[TranscodingError, Value] =
      Right(RealValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Float] =
      value match {
        case NumValue(d) => Right(d.toFloat)
        case _ =>
          Left(
            TranscodingError(s"${value.getType} cannot be converted to real")
          )
      }

    override val typename: String = "float"
  }
  case object DoubleTranscoder extends Transcoder[Double] {
    override def toValue(obj: Double): Either[TranscodingError, Value] =
      Right(RealValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Double] =
      value match {
        case NumValue(d) => Right(d)
        case _ =>
          Left(
            TranscodingError(s"${value.getType} cannot be converted to real")
          )
      }

    override val typename: String = "double"
  }
  case object LongOrDoubleTranscoder extends Transcoder[Either[Long, Double]] {
    override def toValue(
      obj: Either[Long, Double]
    ): Either[TranscodingError, Value] =
      Right(obj match {
        case Left(l)  => IntValue(l)
        case Right(d) => RealValue(d)
      })

    override def fromValue(
      value: Value
    ): Either[TranscodingError, Either[Long, Double]] = value match {
      case IntValue(n)  => Right(Left(n))
      case RealValue(x) => Right(Right(x))
      case _ =>
        Left(
          TranscodingError(s"${value.getType} cannot be converted to number")
        )
    }

    override val typename: String = "number"
  }
  case object BooleanTranscoder extends Transcoder[Boolean] {
    override def toValue(obj: Boolean): Either[TranscodingError, Value] =
      Right(BoolValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, Boolean] =
      value match {
        case BoolValue(b) => Right(b)
        case _ =>
          Left(
            TranscodingError(s"${value.getType} cannot be converted to boolean")
          )
      }

    override val typename: String = "boolean"
  }
  case object StringTranscoder extends Transcoder[String] {
    override def toValue(obj: String): Either[TranscodingError, Value] =
      Right(StringValue(obj))

    override def fromValue(value: Value): Either[TranscodingError, String] =
      value match {
        case StringValue(s) => Right(s)
        case _ =>
          Left(
            TranscodingError(
              s"${value.getType} cannot be converted to string in external function"
            )
          )
      }

    override val typename: String = "string"
  }
  case object HetListTranscoder extends Transcoder[Seq[Value]] {
    override def toValue(obj: Seq[Value]): Either[TranscodingError, Value] =
      Right(ListValue(ArrayBuffer.from(obj)))

    override def fromValue(
      value: Value
    ): Either[TranscodingError, Seq[Value]] = {
      value match {
        case ListValue(l) => Right(l.toSeq)
        case _ =>
          Left(
            TranscodingError(s"${value.getType} cannot be converted to list")
          )
      }
    }

    override val typename: String = "list"
  }
  case object HetArrayBufferTranscoder extends Transcoder[ArrayBuffer[Value]] {
    override def toValue(
      obj: ArrayBuffer[Value]
    ): Either[TranscodingError, Value] =
      Right(ListValue(obj))

    override def fromValue(
      value: Value
    ): Either[TranscodingError, ArrayBuffer[Value]] = {
      value match {
        case ListValue(l) => Right(l)
        case _ =>
          Left(
            TranscodingError(s"${value.getType} cannot be converted to list")
          )
      }
    }

    override val typename: String = "list"
  }
  case object MutableHashMapTranscoder
      extends Transcoder[mutable.HashMap[String, Value]] {
    override def toValue(
      obj: mutable.HashMap[String, Value]
    ): Either[TranscodingError, Value] = Right(ObjectValue(obj))

    override def fromValue(
      value: Value
    ): Either[TranscodingError, mutable.HashMap[String, Value]] = value match {
      case ObjectValue(o) => Right(o)
      case _ =>
        Left(
          TranscodingError(s"${value.getType} cannot be converted to object")
        )
    }

    override val typename: String = "object"
  }
  case object SliceableTranscoder
      extends Transcoder[Either[String, ArrayBuffer[Value]]] {
    override def toValue(
      obj: Either[String, ArrayBuffer[Value]]
    ): Either[TranscodingError, Value] = obj match {
      case Left(s)  => Right(StringValue(s))
      case Right(l) => Right(ListValue(l))
    }

    override def fromValue(
      value: Value
    ): Either[TranscodingError, Either[String, ArrayBuffer[Value]]] =
      value match {
        case StringValue(s) => Right(Left(s))
        case ListValue(l)   => Right(Right(l))
        case _ =>
          Left(
            TranscodingError(
              s"${value.getType} cannot be converted to string or list"
            )
          )
      }

    override val typename: String = "string or list"
  }
  case class OptionalTranscoder[T](underlying: Transcoder[T])
      extends Transcoder[Option[T]] {
    override def toValue(obj: Option[T]): Either[TranscodingError, Value] =
      obj.map(underlying.toValue).getOrElse(Right(NullValue))

    override def fromValue(value: Value): Either[TranscodingError, Option[T]] =
      value match {
        case NullValue => Right(None)
        case _         => underlying.fromValue(value).map(Some(_))
      }

    override val typename: String = s"${underlying.typename} or null"
  }
  case class HomArrayTranscoder[T: ClassTag](t: Transcoder[T])
      extends Transcoder[ArraySeq[T]] {
    override def toValue(obj: ArraySeq[T]): Either[TranscodingError, Value] =
      Utils
        .mapOrFail(obj, t.toValue)
        .map(s => ListValue(ArrayBuffer.from(s)))

    override def fromValue(
      value: Value
    ): Either[TranscodingError, ArraySeq[T]] = {
      value match {
        case ListValue(l) =>
          Utils
            .mapOrFail(l, t.fromValue)
            .map(ArraySeq.from(_))
        case _ =>
          Left(
            TranscodingError(
              s"${value.getType} cannot be converted to list of ${t.typename}"
            )
          )
      }
    }

    override val typename: String = s"list of ${t.typename}"
  }
}
