package flirora.hyphae.interpret

import flirora.hyphae.parse.{InterpreterError, RuntimeError, Value}

sealed trait ExecResult[+T] {
  def map[U](func: T => U): ExecResult[U]
  def flatMap[U](func: T => ExecResult[U]): ExecResult[U]

  def getResult(value: => Value): Either[InterpreterError, Value]
}

case class Success[T](t: T) extends ExecResult[T] {
  override def map[U](func: T => U): ExecResult[U] = Success(func(t))

  override def flatMap[U](func: T => ExecResult[U]): ExecResult[U] =
    func(t) match {
      case u: Success[U] => u
      case fail          => fail
    }

  override def getResult(value: => Value): Either[InterpreterError, Value] =
    Right(value)
}

case class Errored[T](err: RuntimeError) extends ExecResult[T] {
  override def map[U](func: T => U): ExecResult[U] = Errored(err)

  override def flatMap[U](func: T => ExecResult[U]): ExecResult[U] =
    Errored(err)

  override def getResult(value: => Value): Either[InterpreterError, Value] =
    Left(err)
}

case class Returned[T](returnValue: Value) extends ExecResult[T] {
  override def map[U](func: T => U): ExecResult[U] = Returned(returnValue)

  override def flatMap[U](func: T => ExecResult[U]): ExecResult[U] =
    Returned(returnValue)

  override def getResult(value: => Value): Either[InterpreterError, Value] =
    Right(value)
}
