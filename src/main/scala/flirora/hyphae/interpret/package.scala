package flirora.hyphae

import flirora.hyphae.parse._

package object interpret {
  type InterpretExpression = Expression[Int, ScopedBlock]
  type InterpretStore = Store[Int, ScopedBlock]
  type InterpretStatement = Statement[Int, ScopedBlock]
  type InterpretDefinedFunction = DefinedFunction[Int, ScopedBlock]
  type InterpretHFunction = AbstractHFunction[Int, ScopedBlock]
  type InterpretProgram = Program[Int, ScopedBlock]
  type InterpretLibrary = Library[Int, ScopedBlock]
  type Id[T] = T
}
