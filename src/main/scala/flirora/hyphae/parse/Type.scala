package flirora.hyphae.parse

trait Type

case object NullType extends Type {
  override def toString: String = "null"
}

abstract class NumType extends Type

case object IntType extends NumType {
  override def toString: String = "integer"
}

case object RealType extends NumType {
  override def toString: String = "real"
}

case object BoolType extends Type {
  override def toString: String = "boolean"
}

case object StringType extends Type {
  override def toString: String = "string"
}

case object ListType extends Type {
  override def toString: String = "list"
}

case object ObjectType extends Type {
  override def toString: String = "object"
}

case object FunctionType extends Type {
  override def toString: String = "function"
}
