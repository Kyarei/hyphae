package flirora.hyphae.parse

import flirora.hyphae.interpret.{Errored, ExecResult, Success}
import flirora.hyphae.parse.ValueRef.Context

import scala.collection.immutable.TreeMap

abstract class Operator(
    val repr: String,
    val precedence: Int,
    val allowsAssign: Boolean = true
) {}

abstract class EvaluableOperator(
    override val repr: String,
    override val precedence: Int,
    override val allowsAssign: Boolean = true
) extends Operator(repr, precedence, allowsAssign) {
  def eval(a: Value, b: Value): Either[String, Value]

  def evalMutable(a: Value, b: Value): Either[String, Value] = eval(a, b)

  def evalAsResult(a: Value, b: Value, ctx: Context): ExecResult[Value] =
    eval(a, b) match {
      case Right(value) => Success(value)
      case Left(msg) =>
        Errored(RuntimeError(ctx, msg))
    }

  def evalMutableAsResult(a: Value, b: Value, ctx: Context): ExecResult[Value] =
    evalMutable(a, b) match {
      case Right(value) => Success(value)
      case Left(msg) =>
        Errored(RuntimeError(ctx, msg))
    }
}

object Operator {
  abstract case class ArithmeticOperator(
      override val repr: String,
      override val precedence: Int
  ) extends EvaluableOperator(repr, precedence) {

    def onIntegers(a: Long, b: Long): Either[String, Long]
    def onReals(a: Double, b: Double): Either[String, Double]

    override def eval(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (IntValue(a), IntValue(b))   => onIntegers(a, b).map(IntValue)
        case (RealValue(a), RealValue(b)) => onReals(a, b).map(RealValue)
        case (a: ListValue, b: ListValue) => a.broadcast(b, eval)
        case (a: ListValue, b)            => a.broadcastRight(b, eval)
        case (a, b: ListValue)            => b.broadcastLeft(a, eval)
        case _ =>
          Left(
            s"type mismatch: tried to use $repr on ${a.getType} and ${b.getType}"
          )
      }
  }

  object Plus extends ArithmeticOperator("+", 10) {
    override def onIntegers(a: Long, b: Long): Either[String, Long] =
      Right(a + b)

    override def onReals(a: Double, b: Double): Either[String, Double] =
      Right(a + b)

    override def eval(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (StringValue(_), _) | (_, StringValue(_)) =>
          Left(
            s"type mismatch: tried to use $repr on ${a.getType} and ${b.getType} (please use ~ for string concatenation)"
          )
        case _ => super.eval(a, b)
      }
  }

  object Minus extends ArithmeticOperator("-", 10) {
    override def onIntegers(a: Long, b: Long): Either[String, Long] =
      Right(a - b)

    override def onReals(a: Double, b: Double): Either[String, Double] =
      Right(a - b)
  }

  object Times extends ArithmeticOperator("*", 15) {
    override def onIntegers(a: Long, b: Long): Either[String, Long] =
      Right(a * b)

    override def onReals(a: Double, b: Double): Either[String, Double] =
      Right(a * b)

    override def eval(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (StringValue(a), IntValue(b)) =>
          Right(StringValue(a * b.toInt))
        case (IntValue(b), StringValue(a)) =>
          Right(StringValue(a * b.toInt))
        case _ => super.eval(a, b)
      }
  }

  object Div extends ArithmeticOperator("/", 15) {
    override def onIntegers(a: Long, b: Long): Either[String, Long] =
      if (b == 0) Left(s"tried to divide $a by zero") else Right(a / b)

    override def onReals(a: Double, b: Double): Either[String, Double] =
      if (b == 0) Left(s"tried to divide $a by zero") else Right(a / b)
  }

  object Mod extends ArithmeticOperator("%", 15) {
    override def onIntegers(a: Long, b: Long): Either[String, Long] =
      if (b == 0) Left(s"tried to get the remainder $a by zero")
      else Right(a % b)

    override def onReals(a: Double, b: Double): Either[String, Double] =
      if (b == 0) Left(s"tried to get the remainder $a by zero")
      else Right(a % b)
  }

  object Concat extends EvaluableOperator("~", 10) {
    override def eval(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (ListValue(a), ListValue(b))     => Right(ListValue(a ++ b))
        case (ObjectValue(a), ObjectValue(b)) => Right(ObjectValue(a ++ b))
        case (a, b)                           => Right(StringValue(a.stringify + b.stringify))
      }
    override def evalMutable(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (ListValue(a), ListValue(b)) => {
          a ++= b
          Right(ListValue(a))
        }
        case (ObjectValue(a), ObjectValue(b)) => {
          a ++= b
          Right(ObjectValue(a))
        }
        case (a, b) => Right(StringValue(a.stringify + b.stringify))
      }
  }

  object Append extends EvaluableOperator(":~", 10) {
    override def eval(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (ListValue(a), b) => Right(ListValue(a :+ b))
        case _                 => Left(s"tried to append ${b.getType} to ${a.getType}")
      }
    override def evalMutable(a: Value, b: Value): Either[String, Value] =
      (a, b) match {
        case (ListValue(a), b) => {
          a.append(b)
          Right(ListValue(a))
        }
        case _ => Left(s"tried to append ${b.getType} to ${a.getType}")
      }
  }

  object Equal extends EvaluableOperator("==", 8, allowsAssign = false) {
    override def eval(a: Value, b: Value): Either[String, Value] =
      Right(BoolValue(a == b))
  }

  object NotEqual extends EvaluableOperator("!=", 8, allowsAssign = false) {
    override def eval(a: Value, b: Value): Either[String, Value] =
      Right(BoolValue(a != b))
  }

  abstract case class CompareOperator(override val repr: String)
      extends EvaluableOperator(repr, 8, allowsAssign = false) {
    def acceptsCompareStatus(z: Int): Boolean

    override def eval(a: Value, b: Value): Either[String, Value] =
      ((a, b) match {
        case (IntValue(a), IntValue(b)) =>
          Right(acceptsCompareStatus(a.compareTo(b)))
        case (NumValue(a), NumValue(b)) =>
          Right(acceptsCompareStatus(a.compareTo(b)))
        case (StringValue(a), StringValue(b)) =>
          Right(acceptsCompareStatus(a.compareTo(b)))
        case _ => Left(s"tried to use $repr with ${a.getType} and ${b.getType}")
      }).map(BoolValue)
  }

  object Less extends CompareOperator("<") {
    override def acceptsCompareStatus(z: Int): Boolean = z < 0
  }

  object LessEq extends CompareOperator("<=") {
    override def acceptsCompareStatus(z: Int): Boolean = z <= 0
  }

  object Greater extends CompareOperator(">") {
    override def acceptsCompareStatus(z: Int): Boolean = z > 0
  }

  object GreaterEq extends CompareOperator(">=") {
    override def acceptsCompareStatus(z: Int): Boolean = z >= 0
  }

  object LogicalAnd extends Operator("&&", 5)
  object LogicalOr extends Operator("||", 4)

  val ALL_OPERATORS: List[Operator] =
    List(
      Plus,
      Minus,
      Times,
      Div,
      Mod,
      Concat,
      Append,
      Equal,
      NotEqual,
      Less,
      LessEq,
      Greater,
      GreaterEq,
      LogicalAnd,
      LogicalOr
    )

  lazy val operatorsByString: Map[String, Operator] =
    Operator.ALL_OPERATORS
      .map(op => op.repr -> op)
      .toMap

  lazy val operatorsByPrecedence: TreeMap[Int, List[Operator]] =
    TreeMap.from(
      Operator.ALL_OPERATORS
        .groupBy(op => op.precedence)
    )

}
