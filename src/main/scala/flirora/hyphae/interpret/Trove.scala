package flirora.hyphae.interpret

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.reflect.runtime.{universe => ru}

class Trove[M[_]] {
  private val entries: mutable.HashMap[String, ArrayBuffer[(ru.Type, M[_])]] =
    mutable.HashMap()

  def get[T: ru.TypeTag]: Option[M[T]] = {
    val tt = ru.typeTag[T]
    val tpe = tt.tpe.dealias
    entries
      .get(tpe.toString)
      .flatMap(l => l.find(_._1 =:= tpe).map(_._2.asInstanceOf[M[T]]))
  }

  def put[T: ru.TypeTag](entry: M[T], allowOverwrites: Boolean): Unit = {
    val tt = ru.typeTag[T]
    val tpe = tt.tpe.dealias
    val subEntries = entries
      .getOrElseUpdate(tpe.toString, ArrayBuffer())
    subEntries.find(_._1 =:= tpe) match {
      case None => ()
      case Some(_) =>
        if (!allowOverwrites) {
          throw new UnsupportedOperationException(s"Key $tpe already exists")
        } else {
          subEntries.filterInPlace(e => !(e._1 =:= tpe))
        }
    }
    subEntries.append((tpe, entry))
  }
}
